#include "stdafx.h"
#include "Particle.h"
#include <cmath>
// see https://stackoverflow.com/questions/21688529/binary-directxxmvector-does-not-define-this-operator-or-a-conversion
using namespace DirectX;

Particle::Particle(const ParticleEmitterSettings& emitterSettings):
	m_VertexInfo(ParticleVertex()),
	m_EmitterSettings(emitterSettings),
	m_IsActive(false),
	m_TotalEnergy(0),
	m_CurrentEnergy(0),
	m_SizeGrow(0),
	m_InitSize(0)
{}

void Particle::Update(const GameContext& gameContext)
{
	if (!m_IsActive)
	{
		return;
	}
	float dt = gameContext.pGameTime->GetElapsed();

	m_CurrentEnergy -= dt;
	if (m_CurrentEnergy < 0)
	{
		m_IsActive = false;
	}
	DirectX::XMVECTOR posVec = DirectX::XMLoadFloat3(&m_VertexInfo.Position);
	DirectX::XMVECTOR velVec = DirectX::XMLoadFloat3(&m_EmitterSettings.Velocity);
	posVec += velVec * dt;
	DirectX::XMStoreFloat3(&m_VertexInfo.Position, posVec);
	m_VertexInfo.Color = m_EmitterSettings.Color;

	float particleLifePercent =  m_CurrentEnergy / m_TotalEnergy; 
	m_VertexInfo.Color.w = particleLifePercent * 2;

	if (m_SizeGrow >= 1)
	{
		m_VertexInfo.Size = m_InitSize + m_InitSize * m_SizeGrow * (1 - particleLifePercent);
	}
	else
	{
		m_VertexInfo.Size = m_InitSize - m_InitSize * (1 - m_SizeGrow) * (1 - particleLifePercent);
	}

}

void Particle::Init(XMFLOAT3 initPosition)
{
	m_IsActive = true;

	// Energy init
	m_TotalEnergy = randF(m_EmitterSettings.MinEnergy, m_EmitterSettings.MaxEnergy);
	m_CurrentEnergy = m_TotalEnergy;

	// pos init
	DirectX::XMFLOAT3 randDir = DirectX::XMFLOAT3(1, 0, 0);
	DirectX::XMMATRIX rotMat = DirectX::XMMatrixRotationRollPitchYaw(randF(-XM_PI, XM_PI), randF(-XM_PI, XM_PI), randF(-XM_PI, XM_PI));
	DirectX::XMVECTOR randDirVec = DirectX::XMLoadFloat3(&randDir);
	randDirVec = XMVector3TransformNormal(randDirVec, rotMat);
	float distance = randF(m_EmitterSettings.MinEmitterRange, m_EmitterSettings.MaxEmitterRange);
	randDirVec = randDirVec * distance;
	DirectX::XMVECTOR posVec = DirectX::XMLoadFloat3(&initPosition);
	posVec = posVec + distance * randDirVec;
	DirectX::XMStoreFloat3(&m_VertexInfo.Position, posVec);

	// Size init
	m_InitSize = randF(m_EmitterSettings.MinSize, m_EmitterSettings.MaxSize);
	m_VertexInfo.Size = m_InitSize;
	m_SizeGrow = randF(m_EmitterSettings.MinSizeGrow, m_EmitterSettings.MaxSizeGrow);

	// Rotation init
	m_VertexInfo.Rotation = randF(-XM_PI, XM_PI);
}
