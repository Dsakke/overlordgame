//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "ShadowMapMaterial.h"
#include "ContentManager.h"
#include "GeneralStructs.h"

ShadowMapMaterial::~ShadowMapMaterial()
{
	if (m_pInputLayouts[ShadowGenType::Static])
	{
		SafeRelease(m_pInputLayouts[ShadowGenType::Static]);
	}

	if (m_pInputLayouts[ShadowGenType::Skinned])
	{
		SafeRelease(m_pInputLayouts[ShadowGenType::Skinned]);
	}
}

void ShadowMapMaterial::Initialize(const GameContext& gameContext)
{
	if (!m_IsInitialized)
	{
		// Load Effect
		m_pShadowEffect = ContentManager::Load<ID3DX11Effect>(L"Resources/Effects/ShadowMapGenerator.fx");

		// Load Techniques
		m_pShadowTechs[ShadowGenType::Static] = m_pShadowEffect->GetTechniqueByName("GenerateShadows");
		m_pShadowTechs[ShadowGenType::Skinned] = m_pShadowEffect->GetTechniqueByName("GenerateShadows_Skinned");
		// Create InputLayouts
		ILDescription inputLayout{};
		inputLayout.Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputLayout.Offset = 0;
		inputLayout.SemanticType = ILSemantic::POSITION;
		m_InputLayoutDescriptions[ShadowGenType::Static].push_back(inputLayout);

		if (!EffectHelper::BuildInputLayout(gameContext.pDevice, m_pShadowTechs[ShadowGenType::Static], &m_pInputLayouts[ShadowGenType::Static], m_InputLayoutSizes[ShadowGenType::Static]), m_InputLayoutIds[ShadowGenType::Static])
		{
			Logger::LogWarning(L"ShadowMapMaterial::Initialize >> Failed to build static input layout");
		}

		inputLayout.Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputLayout.Offset = 0;
		inputLayout.SemanticType = ILSemantic::POSITION;
		m_InputLayoutDescriptions[ShadowGenType::Skinned].push_back(inputLayout);

		inputLayout.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		inputLayout.Offset = 12;
		inputLayout.SemanticType = ILSemantic::BLENDWEIGHTS;
		m_InputLayoutDescriptions[ShadowGenType::Skinned].push_back(inputLayout);

		inputLayout.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		inputLayout.Offset = 28;
		inputLayout.SemanticType = ILSemantic::BLENDINDICES;
		m_InputLayoutDescriptions[ShadowGenType::Skinned].push_back(inputLayout);

		if (!EffectHelper::BuildInputLayout(gameContext.pDevice, m_pShadowTechs[ShadowGenType::Skinned], &m_pInputLayouts[ShadowGenType::Skinned], m_InputLayoutDescriptions[ShadowGenType::Skinned], m_InputLayoutSizes[ShadowGenType::Skinned], m_InputLayoutIds[ShadowGenType::Skinned]))
		{
			Logger::LogError(L"ShadowMapMaterial::Initialize >> Failed to build skinned input layout");
		}

		m_pWorldMatrixVariable = m_pShadowEffect->GetVariableByName("gWorld")->AsMatrix();
		if (!m_pWorldMatrixVariable->IsValid())
		{
			Logger::LogWarning(L"ShadowMapMaterial::Initialize() >> variable 'gWorld' not found in effect");
		}

		m_pBoneTransforms = m_pShadowEffect->GetVariableByName("gBones")->AsMatrix();
		if (!m_pBoneTransforms->IsValid())
		{
			Logger::LogWarning(L"ShadowMapMaterial::Initialize() >> variable 'gBones' not found in effect");
		}

		m_pLightVPMatrixVariable = m_pShadowEffect->GetVariableByName("gLightViewProj")->AsMatrix();
		if (!m_pLightVPMatrixVariable->IsValid())
		{
			Logger::LogWarning(L"ShadowMapMaterial::Initialize() >> variable 'gLightViewProj' not found in effect");
		}
		m_IsInitialized = true;
	}
}

void ShadowMapMaterial::SetLightVP(DirectX::XMFLOAT4X4 lightVP) const
{
	m_pLightVPMatrixVariable->SetMatrix(reinterpret_cast<float*>(&lightVP));
}

void ShadowMapMaterial::SetWorld(DirectX::XMFLOAT4X4 world) const
{
	m_pWorldMatrixVariable->SetMatrix(reinterpret_cast<float*>(&world));
}

void ShadowMapMaterial::SetBones(const float* pData, int count) const
{
	m_pBoneTransforms->SetMatrixArray(pData, 0, count);
}
