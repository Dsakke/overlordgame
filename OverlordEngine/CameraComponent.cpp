#include "stdafx.h"
#include "CameraComponent.h"
#include "OverlordGame.h"
#include "TransformComponent.h"
#include "PhysxProxy.h"
#include "GameObject.h"
#include "GameScene.h"
#include "SpriteComponent.h"
#include "TextureData.h"

CameraComponent::CameraComponent() :
	m_FarPlane(2500.0f),
	m_NearPlane(0.1f),
	m_FOV(DirectX::XM_PIDIV4),
	m_Size(25.0f),
	m_IsActive(true),
	m_PerspectiveProjection(true)
{
	XMStoreFloat4x4(&m_Projection, DirectX::XMMatrixIdentity());
	XMStoreFloat4x4(&m_View, DirectX::XMMatrixIdentity());
	XMStoreFloat4x4(&m_ViewInverse, DirectX::XMMatrixIdentity());
	XMStoreFloat4x4(&m_ViewProjection, DirectX::XMMatrixIdentity());
	XMStoreFloat4x4(&m_ViewProjectionInverse, DirectX::XMMatrixIdentity());
}

void CameraComponent::Initialize(const GameContext&) {}

void CameraComponent::Update(const GameContext&)
{
	// see https://stackoverflow.com/questions/21688529/binary-directxxmvector-does-not-define-this-operator-or-a-conversion
	using namespace DirectX;

	const auto windowSettings = OverlordGame::GetGameSettings().Window;
	DirectX::XMMATRIX projection;

	if (m_PerspectiveProjection)
	{
		projection = DirectX::XMMatrixPerspectiveFovLH(m_FOV, windowSettings.AspectRatio, m_NearPlane, m_FarPlane);
	}
	else
	{
		const float viewWidth = (m_Size > 0) ? m_Size * windowSettings.AspectRatio : windowSettings.Width;
		const float viewHeight = (m_Size > 0) ? m_Size : windowSettings.Height;
		projection = DirectX::XMMatrixOrthographicLH(viewWidth, viewHeight, m_NearPlane, m_FarPlane);
	}

	const DirectX::XMVECTOR worldPosition = XMLoadFloat3(&GetTransform()->GetWorldPosition());
	const DirectX::XMVECTOR lookAt = XMLoadFloat3(&GetTransform()->GetForward());
	const DirectX::XMVECTOR upVec = XMLoadFloat3(&GetTransform()->GetUp());

	const DirectX::XMMATRIX view = DirectX::XMMatrixLookAtLH(worldPosition, worldPosition + lookAt, upVec);
	const DirectX::XMMATRIX viewInv = XMMatrixInverse(nullptr, view);
	const DirectX::XMMATRIX viewProjectionInv = XMMatrixInverse(nullptr, view * projection);

	XMStoreFloat4x4(&m_Projection, projection);
	XMStoreFloat4x4(&m_View, view);
	XMStoreFloat4x4(&m_ViewInverse, viewInv);
	XMStoreFloat4x4(&m_ViewProjection, view * projection);
	XMStoreFloat4x4(&m_ViewProjectionInverse, viewProjectionInv);
}

void CameraComponent::Draw(const GameContext&) {}

void CameraComponent::SetActive()
{
	auto gameObject = GetGameObject();
	if (gameObject == nullptr)
	{
		Logger::LogError(L"[CameraComponent] Failed to set active camera. Parent game object is null");
		return;
	}

	auto gameScene = gameObject->GetScene();
	if (gameScene == nullptr)
	{
		Logger::LogError(L"[CameraComponent] Failed to set active camera. Parent game scene is null");
		return;
	}

	gameScene->SetActiveCamera(this);
}

GameObject* CameraComponent::Pick(const GameContext& gameContext, CollisionGroupFlag ignoreGroups) const
{
	using namespace DirectX;
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(ignoreGroups);
	
	// calculate NDC coordinates
	POINT screenCoor = gameContext.pInput->GetMousePosition();
	GameSettings::WindowSettings windowSettings = OverlordGame::GetGameSettings().Window;
	float halfWidth = static_cast<float>(windowSettings.Width / 2);
	float halfHeight = static_cast<float>(windowSettings.Height / 2);
	DirectX::XMFLOAT2 ndcCoor = DirectX::XMFLOAT2{ (screenCoor.x - halfWidth) / halfWidth, (halfHeight - screenCoor.y) / halfHeight};


	DirectX::XMMATRIX viewProjectionInverse = DirectX::XMLoadFloat4x4(&m_ViewProjectionInverse);
	// calculate near point
	DirectX::XMFLOAT4 nearPoint = DirectX::XMFLOAT4{ ndcCoor.x, ndcCoor.y, 0, 0 };
	DirectX::XMVECTOR nearVec = DirectX::XMLoadFloat4(&nearPoint);
	nearVec = DirectX::XMVector3TransformCoord(nearVec, viewProjectionInverse);
	DirectX::XMStoreFloat4(&nearPoint, nearVec);

	// calculate far point
	DirectX::XMFLOAT4 farPoint = DirectX::XMFLOAT4{ ndcCoor.x, ndcCoor.y, 1, 0 };
	DirectX::XMVECTOR farVec{};
	farVec = DirectX::XMLoadFloat4(&farPoint);
	farVec = DirectX::XMVector3TransformCoord(farVec, viewProjectionInverse);
	DirectX::XMStoreFloat4(&farPoint, farVec);
	
	physx::PxQueryFilterData filterData{};
	filterData.data.word0 = ~(physx::PxU32)(ignoreGroups); 

	auto physxProxy = GetGameObject()->GetScene()->GetPhysxProxy();
	physx::PxRaycastBuffer hit{};
	DirectX::XMVECTOR dirVec{};
	dirVec = farVec - nearVec;
	dirVec = DirectX::XMVector3Normalize(dirVec);
	physx::PxVec3 dir{ };
	DirectX::XMStoreFloat3(reinterpret_cast<DirectX::XMFLOAT3*>(&dir), dirVec); // PxVec3 and DirectX::XMFLOAT3 have the same member layout so we can reinterpret cast the pointer for the same result

	physx::PxVec3 rayOri{ nearPoint.x, nearPoint.y, nearPoint.z };
	if (physxProxy->Raycast(rayOri, dir, m_FarPlane - m_NearPlane, hit, physx::PxHitFlag::eDEFAULT, filterData))
	{
		return static_cast<BaseComponent*>(hit.block.actor->userData)->GetGameObject();
	}
	return nullptr;
}

GameObject* CameraComponent::PickSprites(const GameContext& gameContext, const std::vector<GameObject*>& pPickableSprites) const
{
	POINT mousePos  = gameContext.pInput->GetMousePosition();
	for (GameObject* pSprite : pPickableSprites)
	{
		SpriteComponent* pSpriteComponent = pSprite->GetComponent<SpriteComponent>();
		if (pSpriteComponent)
		{
			const DirectX::XMFLOAT3 pos = pSprite->GetTransform()->GetPosition();
			const DirectX::XMFLOAT3 scale = pSprite->GetTransform()->GetScale();
			DirectX::XMFLOAT2 pivot = pSpriteComponent->GetPivot();
			DirectX::XMFLOAT2 textureDim = pSpriteComponent->GetTexture()->GetDimension();
			DirectX::XMFLOAT2 bottomLeft;
			bottomLeft.x = pos.x - pivot.x * scale.x * textureDim.x;
			bottomLeft.y = pos.y - pivot.y * scale.y * textureDim.y;
			DirectX::XMFLOAT2 topRight;
			topRight.x = pos.x + (1 - pivot.x) * scale.x * textureDim.x;
			topRight.y = pos.y + (1 - pivot.y) * scale.y * textureDim.y;
			if ((mousePos.x > bottomLeft.x && mousePos.x < topRight.x) && (mousePos.y > bottomLeft.y && mousePos.y < topRight.y))
			{
				return pSprite;
			}
		}
		else
		{
			Logger::LogWarning(L"CameraComponent::PickSprites >> PickableSprite without SpriteComponent passed");
		}
	}
	return nullptr;
}
