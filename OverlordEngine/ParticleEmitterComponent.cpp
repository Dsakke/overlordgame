#include "stdafx.h"
#include "ParticleEmitterComponent.h"
 #include <utility>
#include "EffectHelper.h"
#include "ContentManager.h"
#include "TextureDataLoader.h"
#include "Particle.h"
#include "TransformComponent.h"

ParticleEmitterComponent::ParticleEmitterComponent(std::wstring  assetFile, int particleCount):
	m_pVertexBuffer(nullptr),
	m_pEffect(nullptr),
	m_pParticleTexture(nullptr),
	m_pInputLayout(nullptr),
	m_pInputLayoutSize(0),
	m_Settings(ParticleEmitterSettings()),
	m_ParticleCount(particleCount),
	m_ActiveParticles(0),
	m_LastParticleInit(0.0f),
	m_AssetFile(std::move(assetFile))
{
	for (int i{}; i < m_ParticleCount; ++i)
	{
		m_Particles.push_back(new Particle(m_Settings));
	}
}

ParticleEmitterComponent::~ParticleEmitterComponent()
{
	for (Particle* pPart : m_Particles)
	{
		delete pPart;
	}
	m_pInputLayout->Release();
	m_pVertexBuffer->Release();
}

void ParticleEmitterComponent::Initialize(const GameContext& gameContext)
{
	LoadEffect(gameContext);
	CreateVertexBuffer(gameContext);
	m_pParticleTexture = ContentManager::Load<TextureData>(m_AssetFile);
}

void ParticleEmitterComponent::LoadEffect(const GameContext& gameContext)
{
	m_pEffect = ContentManager::Load<ID3DX11Effect>(L"Resources/Effects/ParticleRenderer.fx"); 
	m_pDefaultTechnique = m_pEffect->GetTechniqueByIndex(0);
	if (!m_pDefaultTechnique->IsValid())
	{
		Logger::LogWarning(L"ParticleEmitterComponent::Initialize >> Failed to retrieve technique");
	}
	m_pWvpVariable = m_pEffect->GetVariableByName("gWorldViewProj")->AsMatrix();
	if (!m_pWvpVariable->IsValid())
	{
		Logger::LogWarning(L"ParticleEmitterComponent::Initialize >> variable 'gWorldViewProj' not found");
	}
	m_pViewInverseVariable = m_pEffect->GetVariableByName("gViewInverse")->AsMatrix();
	if (!m_pViewInverseVariable->IsValid())
	{
		Logger::LogWarning(L"ParticleEmitterComponent::Initialize >> variable 'gViewInverse' not found");
	}
	m_pTextureVariable = m_pEffect->GetVariableByName("gParticleTexture")->AsShaderResource();
	if (!m_pTextureVariable->IsValid())
	{
		Logger::LogWarning(L"ParticleEmitterComponent::Initialize >> variable 'gParticleTexture' not found");
	}
	if (!EffectHelper::BuildInputLayout(gameContext.pDevice, m_pDefaultTechnique, &m_pInputLayout, m_pInputLayoutSize))
	{
		Logger::LogWarning(L"ParticleEmitterComponent::Initialize >> Failed to build input layout");
	}
}

void ParticleEmitterComponent::CreateVertexBuffer(const GameContext& gameContext)
{
	if (m_pVertexBuffer)
	{
		m_pVertexBuffer->Release();
	}
	D3D11_BUFFER_DESC desc{};
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.ByteWidth = m_ParticleCount * sizeof(ParticleVertex);
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.MiscFlags = 0;
	HRESULT result = gameContext.pDevice->CreateBuffer(&desc, nullptr, &m_pVertexBuffer);
	if (result != S_OK)
	{
		Logger::LogError(L"ParticleEmitterComponent::CreateVertexBuffer >> Failed to create vertex buffer");
	}
}

void ParticleEmitterComponent::Update(const GameContext& gameContext)
{
	const float particleInterval = ((m_Settings.MinEnergy + m_Settings.MaxEnergy) / 2) / m_ParticleCount;
	m_LastParticleInit += gameContext.pGameTime->GetElapsed();
	m_ActiveParticles = 0;
	D3D11_MAPPED_SUBRESOURCE subresource{};
	gameContext.pDeviceContext->Map(m_pVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &subresource);
	ParticleVertex* pBuffer = reinterpret_cast<ParticleVertex*>(subresource.pData);
	for (Particle* pPart : m_Particles)
	{
		pPart->Update(gameContext);
		if (pPart->IsActive())
		{
			pBuffer[m_ActiveParticles] = pPart->GetVertexInfo();
			++m_ActiveParticles;
		}
		else if (m_LastParticleInit > particleInterval)
		{
			pPart->Init(GetTransform()->GetPosition());
			pBuffer[m_ActiveParticles] = pPart->GetVertexInfo();
			++m_ActiveParticles;
			m_LastParticleInit -= particleInterval;
		}
	}
	gameContext.pDeviceContext->Unmap(m_pVertexBuffer, 0);

	//BUFFER MAPPING CODE [PARTIAL :)]
	//D3D11_MAPPED_SUBRESOURCE mappedResource;
	//gameContext.pDeviceContext->Map(m_pVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	//ParticleVertex* pBuffer = (ParticleVertex*) mappedResource.pData;
}

void ParticleEmitterComponent::Draw(const GameContext& )
{}

void ParticleEmitterComponent::PostDraw(const GameContext& gameContext)
{
	const DirectX::XMFLOAT4X4& vp = gameContext.pCamera->GetViewProjection();
	const DirectX::XMFLOAT4X4& w = GetTransform()->GetWorld();

	DirectX::XMMATRIX vpMat = DirectX::XMLoadFloat4x4(&vp);
	DirectX::XMMATRIX wMat = DirectX::XMLoadFloat4x4(&w);
	DirectX::XMMATRIX wvpMat = wMat * vpMat;

	DirectX::XMFLOAT4X4 wvp{};
	DirectX::XMStoreFloat4x4(&wvp, wvpMat);
	m_pWvpVariable->SetMatrix(reinterpret_cast<const float*>(&wvp));

	m_pViewInverseVariable->SetMatrix(reinterpret_cast<const float*>(&gameContext.pCamera->GetViewInverse()));
	m_pTextureVariable->SetResource(m_pParticleTexture->GetShaderResourceView());

	gameContext.pDeviceContext->IASetInputLayout(m_pInputLayout);
	gameContext.pDeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	UINT stride = sizeof(ParticleVertex);
	UINT offset = 0;
	gameContext.pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	D3DX11_TECHNIQUE_DESC desc;
	m_pDefaultTechnique->GetDesc(&desc);
	for (uint32_t i{}; i < desc.Passes; ++i)
	{
		m_pDefaultTechnique->GetPassByIndex(i)->Apply(0, gameContext.pDeviceContext);
		gameContext.pDeviceContext->DrawIndexed(m_ActiveParticles, 0, 0);
	}
}
