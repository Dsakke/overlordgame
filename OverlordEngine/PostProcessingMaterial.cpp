#include "stdafx.h"
#include "PostProcessingMaterial.h"
#include "RenderTarget.h"
#include "OverlordGame.h"
#include "ContentManager.h"
#include "SceneManager.h"

PostProcessingMaterial::PostProcessingMaterial(std::wstring effectFile, unsigned int renderIndex,
                                               std::wstring technique)
	: m_IsInitialized(false), 
	  m_pInputLayout(nullptr),
	  m_pInputLayoutSize(0),
	  m_effectFile(std::move(effectFile)),
	  m_InputLayoutID(0),
	  m_RenderIndex(renderIndex),
	  m_pRenderTarget(nullptr),
	  m_pVertexBuffer(nullptr),
	  m_pIndexBuffer(nullptr),
	  m_NumVertices(0),
	  m_NumIndices(0),
	  m_VertexBufferStride(sizeof(VertexPosTex)),
	  m_pEffect(nullptr),
	  m_pTechnique(nullptr),
	  m_TechniqueName(std::move(technique))
{
}

PostProcessingMaterial::~PostProcessingMaterial()
{
	SafeDelete(m_pRenderTarget);
	SafeRelease(m_pVertexBuffer);
	SafeRelease(m_pIndexBuffer);
	SafeRelease(m_pInputLayout);
}

void PostProcessingMaterial::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	if (!m_IsInitialized)
	{
		//TODO: complete
		//1. LoadEffect (LoadEffect(...))
		LoadEffect(gameContext, m_effectFile);
		//2. CreateInputLaytout (CreateInputLayout(...))
		//   CreateVertexBuffer (CreateVertexBuffer(...)) > As a TriangleStrip (FullScreen Quad)
		if (!EffectHelper::BuildInputLayout(gameContext.pDevice, m_pTechnique, &m_pInputLayout, m_pInputLayoutSize))
		{
			Logger::LogWarning(L"PostProcessingMaterial::Initialize >> Failed to create input layout");
		}
		CreateVertexBuffer(gameContext);
		CreateIndexBuffer(gameContext);
		//3. Create RenderTarget (m_pRenderTarget)
		//		Take a look at the class, figure out how to initialize/create a RenderTarget Object
		//		GameSettings > OverlordGame::GetGameSettings()
		m_pRenderTarget = new RenderTarget(gameContext.pDevice);
		RENDERTARGET_DESC rtDesc{};
		const GameSettings& settings = OverlordGame::GetGameSettings();
		rtDesc.Height = settings.Window.Height;
		rtDesc.Width = settings.Window.Width;
		rtDesc.EnableColorSRV = true;
		rtDesc.GenerateMipMaps_Color = true;
		m_pRenderTarget->Create(rtDesc);
		m_IsInitialized = true;
	}
}

bool PostProcessingMaterial::LoadEffect(const GameContext& gameContext, const std::wstring& effectFile)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(effectFile);

	//TODO: complete
	//Load Effect through ContentManager
	//Check if m_TechniqueName (default constructor parameter) is set
	// If SET > Use this Technique (+ check if valid)
	// If !SET > Use Technique with index 0
	m_pEffect = ContentManager::Load<ID3DX11Effect>(m_effectFile);
	if (!m_pEffect)
	{
		Logger::LogWarning(L"PostProcessingMaterial::LoadEffect >> Failed to load m_pEffect");
		return false;
	}
	m_pTechnique = m_pEffect->GetTechniqueByIndex(0);
	if (!m_pTechnique->IsValid())
	{
		Logger::LogWarning(L"PostProcessingMaterial::LoadEffect >> Failed to get technique");
		return false;
	}
	//Call LoadEffectVariables
	LoadEffectVariables();
	return true;
}

void PostProcessingMaterial::Draw(const GameContext& gameContext, RenderTarget* previousRendertarget)
{

	//TODO: complete
	//1. Clear the object's RenderTarget (m_pRenderTarget) [Check RenderTarget Class]
	float clearColor[4]{ 0.f,0.f,0.f,0.f };
	m_pRenderTarget->Clear(gameContext, clearColor);
	//2. Call UpdateEffectVariables(...)
	UpdateEffectVariables(previousRendertarget);
	gameContext.pDeviceContext->IASetInputLayout(m_pInputLayout);
	//3. Set InputLayout
	gameContext.pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT::DXGI_FORMAT_R32_UINT, 0);
	UINT offset = 0;
	//4. Set VertexBuffer
	gameContext.pDeviceContext->IASetVertexBuffers(0, 1 , &m_pVertexBuffer, &m_VertexBufferStride, &offset);
	//5. Set PrimitiveTopology (TRIANGLELIST)
	gameContext.pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//6. Draw 
	D3DX11_TECHNIQUE_DESC techDesc{};
	m_pTechnique->GetDesc(&techDesc);
	for (uint32_t i{}; i < techDesc.Passes; ++i)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, gameContext.pDeviceContext);
		gameContext.pDeviceContext->DrawIndexed(6, 0, 0);
	}
	// Generate Mips
	gameContext.pDeviceContext->GenerateMips(m_pRenderTarget->GetShaderResourceView());
	ClearEffectVariables();

	m_pTechnique->GetPassByIndex(0)->Apply(0, gameContext.pDeviceContext);
	
}

void PostProcessingMaterial::CreateVertexBuffer(const GameContext& gameContext)
{
	m_NumVertices = 4;

	D3D11_BUFFER_DESC buffDesc{};
	buffDesc.ByteWidth = sizeof(VertexPosTex) * 4;
	buffDesc.Usage = D3D11_USAGE::D3D11_USAGE_IMMUTABLE;
	buffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;
	const VertexPosTex vertexes[4] =
	{
		{ { -1.f,     1.f, 0.f }, { 0.f, 0.f } }, // top left
		{ {  1.f,     1.f, 0.f }, { 1.f, 0.f } }, // top right
		{ { -1.f,   -1.f, 0.f }, { 0.f, 1.f } }, // bottom left
		{ {  1.f,   -1.f, 0.f }, { 1.f, 1.f } }  // bottom right
	};
	D3D11_SUBRESOURCE_DATA data{};
	data.pSysMem = vertexes;
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;
	//fill a buffer description to copy the vertexdata into graphics memory
	HRESULT result = gameContext.pDevice->CreateBuffer(&buffDesc, &data, &m_pVertexBuffer);
	if (result != S_OK)
	{
		Logger::LogWarning(L"Failed to create vertex buffer");
	}
}

void PostProcessingMaterial::CreateIndexBuffer(const GameContext& gameContext)
{
	m_NumIndices = 6;

	D3D11_BUFFER_DESC buffDesc{};
	buffDesc.ByteWidth = sizeof(uint32_t) * 6;
	buffDesc.Usage = D3D11_USAGE::D3D11_USAGE_IMMUTABLE;
	buffDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	uint32_t indexes[6]{ 0,1,2,2,1,3 };
	D3D11_SUBRESOURCE_DATA data{};
	data.pSysMem = indexes;
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;
	HRESULT result = gameContext.pDevice->CreateBuffer(&buffDesc, &data, &m_pIndexBuffer);
	if (result != S_OK)
	{
		Logger::LogWarning(L"Failed to create index buffer");
	}
}
