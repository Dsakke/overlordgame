#include "stdafx.h"
#include "ModelAnimator.h"

ModelAnimator::ModelAnimator(MeshFilter* pMeshFilter):
m_pMeshFilter(pMeshFilter),
m_Transforms(std::vector<DirectX::XMFLOAT4X4>()),
m_IsPlaying(false), 
m_Reversed(false),
m_ClipSet(false),
m_TickCount(0),
m_AnimationSpeed(1.0f)
{
	SetAnimation(0);
}

void ModelAnimator::SetAnimation(UINT clipNumber)
{
	//TODO: complete
	//Set m_ClipSet to false
	m_ClipSet = false;
	//Check if clipNumber is smaller than the actual m_AnimationClips vector size
	if (m_pMeshFilter->m_AnimationClips.empty())
	{
		return;
	}
	if (m_pMeshFilter->m_AnimationClips.size() < clipNumber)
	{
		Reset();
		Logger::LogWarning(L"ModelAnimator::SetAnimation(UINT) called with clip number which was out of range");
	}
	else
	{
		SetAnimation(m_pMeshFilter->m_AnimationClips[clipNumber]);
	}
	//If not,
		//	Call Reset
		//	Log a warning with an appropriate message
		//	return
	//else
		//	Retrieve the AnimationClip from the m_AnimationClips vector based on the given clipNumber
		//	Call SetAnimation(AnimationClip clip)
}

void ModelAnimator::SetAnimation(std::wstring clipName)
{
	//TODO: complete
	//Set m_ClipSet to false
	//Iterate the m_AnimationClips vector and search for an AnimationClip with the given name (clipName)
	//If found,
	//	Call SetAnimation(Animation Clip) with the found clip
	//Else
	//	Call Reset
	//	Log a warning with an appropriate message
	m_ClipSet = false;
	auto searchLambda = [=](const AnimationClip& aniClip) { return aniClip.Name == clipName; };
	auto it = std::find_if(m_pMeshFilter->m_AnimationClips.begin(), m_pMeshFilter->m_AnimationClips.end(), searchLambda);
	if (it != m_pMeshFilter->m_AnimationClips.end())
	{
		SetAnimation(*it);
	}
	else
	{
		Reset();
		Logger::LogWarning(L"ModelAnimator::SetAnimation(std::wstring) could not find animation clip: " + clipName);
	}
}

void ModelAnimator::SetAnimation(AnimationClip clip)
{
	m_ClipSet = true;
	m_CurrentClip = clip;
	Reset(false);
}

void ModelAnimator::Reset(bool pause)
{
	UNREFERENCED_PARAMETER(pause);
	//TODO: complete
	//If pause is true, set m_IsPlaying to false

	//Set m_TickCount to zero
	//Set m_AnimationSpeed to 1.0f

	//If m_ClipSet is true
	//	Retrieve the BoneTransform from the first Key from the current clip (m_CurrentClip)
	//	Refill the m_Transforms vector with the new BoneTransforms (have a look at vector::assign)
	//Else
	//	Create an IdentityMatrix 
	//	Refill the m_Transforms vector with this IdenityMatrix (Amount = BoneCount) (have a look at vector::assign)
	m_IsPlaying = !pause;
	m_TickCount = 0;
	m_AnimationSpeed = 1.f;
	if (m_ClipSet)
	{
		const std::vector<DirectX::XMFLOAT4X4>& boneTransforms = m_CurrentClip.Keys[0].BoneTransforms;
		m_Transforms.assign(boneTransforms.begin(), boneTransforms.end());
	}
	else
	{
		m_Transforms.assign(m_pMeshFilter->m_BoneCount, DirectX::XMFLOAT4X4{ 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 });
	}
}

void ModelAnimator::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//TODO: complete
	//We only update the transforms if the animation is running and the clip is set
	if (m_IsPlaying && m_ClipSet)
	{
		//1. 
		//Calculate the passedTicks (see the lab document)
		//auto passedTicks = ...
		//Make sure that the passedTicks stay between the m_CurrentClip.Duration bounds (fmod)
		float passedTicks = gameContext.pGameTime->GetElapsed() * m_CurrentClip.TicksPerSecond * m_AnimationSpeed;
		passedTicks = fmod(passedTicks, m_CurrentClip.Duration);

		//2. 
		//IF m_Reversed is true
		//	Subtract passedTicks from m_TickCount
		//	If m_TickCount is smaller than zero, add m_CurrentClip.Duration to m_TickCount
		//ELSE
		//	Add passedTicks to m_TickCount
		//	if m_TickCount is bigger than the clip duration, subtract the duration from m_TickCount
		if (m_Reversed)
		{
			m_TickCount -= passedTicks;
			if (m_TickCount < 0)
			{
				m_TickCount += m_CurrentClip.Duration;
			}
		}
		else
		{
			m_TickCount += passedTicks;
			if (m_TickCount > m_CurrentClip.Duration)
			{
				m_TickCount -= m_CurrentClip.Duration;
			}
		}
		//3.
		//Find the enclosing keys
		AnimationKey keyA, keyB;
		//Iterate all the keys of the clip and find the following keys:
		//keyA > Closest Key with Tick before/smaller than m_TickCount
		//keyB > Closest Key with Tick after/bigger than m_TickCount
		for (AnimationKey& key : m_CurrentClip.Keys)
		{
			if (key.Tick < m_TickCount)
			{
				keyA = key;
			}
			else
			{
				keyB = key;
				break;
			}
		}

		//4.
		//Interpolate between keys
		//Figure out the BlendFactor (See lab document)
		//Clear the m_Transforms vector
		//FOR every boneTransform in a key (So for every bone)
		//	Retrieve the transform from keyA (transformA)
		//	auto transformA = ...
		// 	Retrieve the transform from keyB (transformB)
		//	auto transformB = ...
		//	Decompose both transforms
		//	Lerp between all the transformations (Position, Scale, Rotation)
		//	Compose a transformation matrix with the lerp-results
		//	Add the resulting matrix to the m_Transforms vector
		float blendFactor = (m_TickCount - keyA.Tick) / (keyB.Tick - keyA.Tick);
		for (USHORT i{}; i < m_pMeshFilter->m_BoneCount; ++i)
		{ 
			DirectX::XMMATRIX matA = DirectX::XMLoadFloat4x4(&keyA.BoneTransforms[i]);
			DirectX::XMVECTOR scaleA{};
			DirectX::XMVECTOR rotA{};
			DirectX::XMVECTOR transA{};
			DirectX::XMMatrixDecompose(&scaleA, &rotA, &transA, matA);

			DirectX::XMMATRIX matB = DirectX::XMLoadFloat4x4(&keyB.BoneTransforms[i]);
			DirectX::XMVECTOR scaleB{};
			DirectX::XMVECTOR rotB{};
			DirectX::XMVECTOR transB{};
			DirectX::XMMatrixDecompose(&scaleB, &rotB, &transB, matB);

			DirectX::XMMATRIX rotMat = DirectX::XMMatrixRotationQuaternion(DirectX::XMQuaternionSlerp(rotA, rotB, blendFactor));
			DirectX::XMMATRIX scaleMat = DirectX::XMMatrixScalingFromVector(DirectX::XMVectorLerp(scaleA, scaleB, blendFactor));
			DirectX::XMMATRIX transMat = DirectX::XMMatrixTranslationFromVector(DirectX::XMVectorLerp(transA, transB, blendFactor));

			DirectX::XMMATRIX mat = scaleMat * rotMat * transMat;
			DirectX::XMStoreFloat4x4(&m_Transforms[i], mat);
		}
		
	}
}
