#include "stdafx.h"
#include "ShadowMapRenderer.h"
#include "ContentManager.h"
#include "ShadowMapMaterial.h"
#include "RenderTarget.h"
#include "MeshFilter.h"
#include "SceneManager.h"
#include "OverlordGame.h"
#include"GeneralStructs.h"

ShadowMapRenderer::~ShadowMapRenderer()
{
	delete m_pShadowMat;
	delete m_pShadowRT;
}

void ShadowMapRenderer::Initialize(const GameContext& gameContext)
{
	if (m_IsInitialized)
		return;

	//TODO: create shadow generator material + initialize it
	m_pShadowMat = new ShadowMapMaterial();
	m_pShadowMat->Initialize(gameContext);
	//TODO: create a rendertarget with the correct settings (hint: depth only) for the shadow generator using a RENDERTARGET_DESC
	m_pShadowRT = new RenderTarget(gameContext.pDevice);
	RENDERTARGET_DESC rtDesc{};
	rtDesc.ColorBufferSupplied = false;
	rtDesc.DepthBufferSupplied = false;
	rtDesc.EnableColorBuffer = false;
	rtDesc.EnableColorSRV = false;
	rtDesc.GenerateMipMaps_Color = false;
	rtDesc.Width = static_cast<int>(SceneManager::GetInstance()->GetGame()->GetGameSettings().Window.Width);
	rtDesc.Height = static_cast<int>(SceneManager::GetInstance()->GetGame()->GetGameSettings().Window.Height);
	rtDesc.EnableDepthSRV = true;
	rtDesc.EnableDepthBuffer = true;

	m_pShadowRT->Create(rtDesc);
	m_IsInitialized = true;
}

void ShadowMapRenderer::SetLight(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 direction)
{
	using namespace DirectX;

	m_LightDirection = direction;
	m_LightPosition = position;

	DirectX::XMVECTOR lightPos{ DirectX::XMLoadFloat3(&m_LightPosition) };
	DirectX::XMVECTOR lightDir{ DirectX::XMLoadFloat3(&m_LightDirection) };

	DirectX::XMMATRIX view{ DirectX::XMMatrixLookAtLH(lightPos, lightPos + lightDir, DirectX::XMVECTOR{0.f,1.f,0.f}) };
	DirectX::XMMATRIX proj{ DirectX::XMMatrixOrthographicLH(m_Size * SceneManager::GetInstance()->GetGame()->GetGameSettings().Window.AspectRatio, m_Size, FLT_EPSILON, 500) };
	DirectX::XMStoreFloat4x4(&m_LightVP, view * proj);
	m_pShadowMat->SetLightVP(m_LightVP);
}

void ShadowMapRenderer::Begin(const GameContext& gameContext) const
{
	//Reset Texture Register 5 (Unbind)
	ID3D11ShaderResourceView *const pSRV[] = { nullptr };
	gameContext.pDeviceContext->PSSetShaderResources(1, 1, pSRV);

	SceneManager::GetInstance()->GetGame()->SetRenderTarget(m_pShadowRT);
	m_pShadowRT->Clear(gameContext, nullptr);

	m_pShadowMat->SetLightVP(m_LightVP);
}

void ShadowMapRenderer::End(const GameContext& gameContext) const
{
	UNREFERENCED_PARAMETER(gameContext);
	SceneManager::GetInstance()->GetGame()->SetRenderTarget(nullptr);

}

void ShadowMapRenderer::Draw(const GameContext& gameContext, MeshFilter* pMeshFilter, DirectX::XMFLOAT4X4 world, const std::vector<DirectX::XMFLOAT4X4>& bones) const
{
	//TODO: update shader variables in material
	//TODO: set the correct inputlayout, buffers, topology (some variables are set based on the generation type Skinned or Static)
	//TODO: invoke draw call
	m_pShadowMat->SetWorld(world);
	m_pShadowMat->SetBones(reinterpret_cast<const float*>(bones.data()), bones.size());

	ShadowMapMaterial::ShadowGenType genType{ pMeshFilter->HasElement(ILSemantic::BLENDINDICES) ? ShadowMapMaterial::ShadowGenType::Skinned : ShadowMapMaterial::ShadowGenType::Static};

	gameContext.pDeviceContext->IASetInputLayout(m_pShadowMat->m_pInputLayouts[genType]);

	UINT offset{ 0 };
	gameContext.pDeviceContext->IASetVertexBuffers
	(
		0,
		1,
		&pMeshFilter->m_VertexBuffers[0].pVertexBuffer,
		&pMeshFilter->m_VertexBuffers[0].VertexStride,
		&offset
	);

	gameContext.pDeviceContext->IASetIndexBuffer(pMeshFilter->m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	gameContext.pDeviceContext->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	D3DX11_TECHNIQUE_DESC techDesc{};
	m_pShadowMat->m_pShadowTechs[genType]->GetDesc(&techDesc);
	for (unsigned int i = 0; i < techDesc.Passes; ++i)
	{
		m_pShadowMat->m_pShadowTechs[genType]->GetPassByIndex(i)->Apply(0, gameContext.pDeviceContext);
		gameContext.pDeviceContext->DrawIndexed(pMeshFilter->m_IndexCount, 0, 0);
	}
}

void ShadowMapRenderer::UpdateMeshFilter(const GameContext& gameContext, MeshFilter* pMeshFilter)
{
	if (pMeshFilter->HasElement(ILSemantic::BLENDINDICES))
	{
		pMeshFilter->BuildVertexBuffer
		(
			gameContext,
			m_pShadowMat->m_InputLayoutIds[ShadowMapMaterial::ShadowGenType::Skinned],
			m_pShadowMat->m_InputLayoutSizes[ShadowMapMaterial::ShadowGenType::Skinned],
			m_pShadowMat->m_InputLayoutDescriptions[ShadowMapMaterial::ShadowGenType::Skinned]
		);
	}
	else
	{
		pMeshFilter->BuildVertexBuffer
		(
			gameContext,
			m_pShadowMat->m_InputLayoutIds[ShadowMapMaterial::ShadowGenType::Static],
			m_pShadowMat->m_InputLayoutSizes[ShadowMapMaterial::ShadowGenType::Static],
			m_pShadowMat->m_InputLayoutDescriptions[ShadowMapMaterial::ShadowGenType::Static]
		);
	}
}

ID3D11ShaderResourceView* ShadowMapRenderer::GetShadowMap() const
{
	//TODO: return the depth shader resource view of the shadow generator render target
	return m_pShadowRT->GetDepthShaderResourceView();
}
