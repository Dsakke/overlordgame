#include "stdafx.h"

#include "SpriteComponent.h"
 #include <utility>

#include "GameObject.h"
#include "TextureData.h"
#include "ContentManager.h"
#include "SpriteRenderer.h"
#include "TransformComponent.h"
#include "GameObject.h"

SpriteComponent::SpriteComponent(std::wstring spriteAsset, DirectX::XMFLOAT2 pivot, DirectX::XMFLOAT4 color) :
	m_pTexture(nullptr),
	m_SpriteAsset(std::move(spriteAsset)),
	m_Pivot(pivot),
	m_Color(color), 
	m_IsActive{true}
{
}

void SpriteComponent::Initialize(const GameContext& )
{
	m_pTexture = ContentManager::Load<TextureData>(m_SpriteAsset);
}

const TextureData* SpriteComponent::GetTexture() const
{
	return m_pTexture;
}

bool SpriteComponent::GetActive() const
{
	return m_IsActive;
}

void SpriteComponent::SetActive(bool active)
{
	m_IsActive = active;
}

void SpriteComponent::SetTexture(const std::wstring& spriteAsset)
{
	m_SpriteAsset = spriteAsset;
	m_pTexture = ContentManager::Load<TextureData>(m_SpriteAsset);
}

void SpriteComponent::Update(const GameContext& )
{
}

void SpriteComponent::Draw(const GameContext&)
{
	if (!m_pTexture || !m_IsActive)
		return;

	//TODO: Here you need to draw the SpriteComponent using the Draw of the sprite renderer
	// The sprite renderer is a singleton
	// you will need to position, the rotation and the scale
	// You can use the QuaternionToEuler function to help you with the z rotation 

	DirectX::XMFLOAT4 rot{ GetGameObject()->GetTransform()->GetRotation() };
	DirectX::XMFLOAT3 pos{ GetGameObject()->GetTransform()->GetPosition() };
	DirectX::XMFLOAT3 scale{ GetGameObject()->GetTransform()->GetScale() };
	SpriteVertex vert{};
	vert.Color = m_Color;
	vert.TransformData = DirectX::XMFLOAT4{ pos.x, pos.y, pos.z, QuaternionToEuler(rot).z };
	vert.TransformData2 = DirectX::XMFLOAT4{ m_Pivot.x, m_Pivot.y, scale.x, scale.y };
	SpriteRenderer::GetInstance()->Draw(m_pTexture, DirectX::XMFLOAT2{ pos.x, pos.y }, m_Color, m_Pivot, DirectX::XMFLOAT2{ scale.x, scale.y }, rot.z, pos.z);
}
