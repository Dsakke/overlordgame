#include "stdafx.h"
#include "SpriteFontLoader.h"
#include "BinaryReader.h"
#include "ContentManager.h"
#include "TextureData.h"

SpriteFont* SpriteFontLoader::LoadContent(const std::wstring& assetFile)
{
	auto pBinReader = new BinaryReader();
	pBinReader->Open(assetFile);

	if (!pBinReader->Exists())
	{
		Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Failed to read the assetFile!\nPath: \'%s\'", assetFile.c_str());
		return nullptr;
	}

	//See BMFont Documentation for Binary Layout

	//Parse the Identification bytes (B,M,F)
	//If Identification bytes doesn't match B|M|F,
	//Log Error (SpriteFontLoader::LoadContent > Not a valid .fnt font) &
	//return nullptr
	//...
	bool isBMF{true};
	isBMF = isBMF && (pBinReader->Read<char>() == 'B');
	isBMF = isBMF && (pBinReader->Read<char>() == 'M');
	isBMF = isBMF && (pBinReader->Read<char>() == 'F');
	

	if (!isBMF)
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > Not a valid .fnt font");
		return nullptr;
	}

	//Parse the version (version 3 required)
	//If version is < 3,
	//Log Error (SpriteFontLoader::LoadContent > Only .fnt version 3 is supported)
	//return nullptr
	//...
	if (pBinReader->Read<char>() != 3)
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > Only .fnt version 3 is supported");
	}

	//Valid .fnt file
	auto pSpriteFont = new SpriteFont();
	//SpriteFontLoader is a friend class of SpriteFont
	//That means you have access to its privates (pSpriteFont->m_FontName = ... is valid)
	
	//**********
	// BLOCK 0 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve the FontSize (will be -25, BMF bug) [SpriteFont::m_FontSize]
	//Move the binreader to the start of the FontName [BinaryReader::MoveBufferPosition(...) or you can set its position using BinaryReader::SetBufferPosition(...))
	//Retrieve the FontName [SpriteFont::m_FontName]
	//...
	char blockId0{ pBinReader->Read<char>() };
	int blockSize0{ pBinReader->Read<int>() };
	UNREFERENCED_PARAMETER(blockId0);
	UNREFERENCED_PARAMETER(blockSize0);
	short fontSize{pBinReader->Read<short>()};
	pSpriteFont->m_FontSize = fontSize;
	pBinReader->MoveBufferPosition(12);
	pSpriteFont->m_FontName = pBinReader->ReadNullString();


	//**********
	// BLOCK 1 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve Texture Width & Height [SpriteFont::m_TextureWidth/m_TextureHeight]
	//Retrieve PageCount
	//> if pagecount > 1
	//> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed)
	//Advance to Block2 (Move Reader)
	//...
	char blockId1{ pBinReader->Read<char>() };
	int blockSize1{ pBinReader->Read<int>() };
	UNREFERENCED_PARAMETER(blockId1);
	UNREFERENCED_PARAMETER(blockSize1);
	pBinReader->MoveBufferPosition(4);
	pSpriteFont->m_TextureWidth = pBinReader->Read<uint16_t>();
	pSpriteFont->m_TextureHeight = pBinReader->Read<uint16_t>();
	uint16_t pageCount = pBinReader->Read<uint16_t>();
	if (pageCount > 1)
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed");
	}
	pBinReader->MoveBufferPosition(5);

	//**********
	// BLOCK 2 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve the PageName (store Local)
	//	> If PageName is empty
	//	> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty])
	//>Retrieve texture filepath from the assetFile path
	//> (ex. c:/Example/somefont.fnt => c:/Example/) [Have a look at: wstring::rfind()]
	//>Use path and PageName to load the texture using the ContentManager [SpriteFont::m_pTexture]
	//> (ex. c:/Example/ + 'PageName' => c:/Example/somefont_0.png)
	//...
	char blockId2{ pBinReader->Read<char>() };
	int blockSize2{ pBinReader->Read<int>() };
	UNREFERENCED_PARAMETER(blockId2);
	UNREFERENCED_PARAMETER(blockSize2);
	std::wstring pageName = pBinReader->ReadNullString();
	if (pageName.empty())
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty]");
	}
	std::wstring pathToPage{assetFile.substr(0, assetFile.rfind(L'/') + 1) + pageName};
	pSpriteFont->m_pTexture = ContentManager::Load<TextureData>(pathToPage);
	

	//**********
	// BLOCK 3 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve Character Count (see documentation)
	//Retrieve Every Character, For every Character:
	//> Retrieve CharacterId (store Local) and cast to a 'wchar_t'
	//> Check if CharacterId is valid (SpriteFont::IsCharValid), Log Warning and advance to next character if not valid
	//> Retrieve the corresponding FontMetric (SpriteFont::GetMetric) [REFERENCE!!!]
	//> Set IsValid to true [FontMetric::IsValid]
	//> Set Character (CharacterId) [FontMetric::Character]
	//> Retrieve Xposition (store Local)
	//> Retrieve Yposition (store Local)
	//> Retrieve & Set Width [FontMetric::Width]
	//> Retrieve & Set Height [FontMetric::Height]
	//> Retrieve & Set OffsetX [FontMetric::OffsetX]
	//> Retrieve & Set OffsetY [FontMetric::OffsetY]
	//> Retrieve & Set AdvanceX [FontMetric::AdvanceX]
	//> Retrieve & Set Page [FontMetric::Page]
	//> Retrieve Channel (BITFIELD!!!) 
	//	> See documentation for BitField meaning [FontMetrix::Channel]
	//> Calculate Texture Coordinates using Xposition, Yposition, TextureWidth & TextureHeight [FontMetric::TexCoord]
	//...

	char blockId3{ pBinReader->Read<char>() };
	UNREFERENCED_PARAMETER(blockId3);
	int blockSize3{ pBinReader->Read<int>() };

	const int charSizeBlock{ 20 };

	int nrChars = blockSize3 / charSizeBlock;
	pSpriteFont->m_CharacterCount = nrChars;
	for (int i{}; i < nrChars; ++i)
	{
		wchar_t charId{ (wchar_t)pBinReader->Read<uint32_t>() };
		if (pSpriteFont->IsCharValid(charId))
		{
			FontMetric& fontMetric{ pSpriteFont->GetMetric(charId) };
			fontMetric.Character = charId;
			unsigned short x = pBinReader->Read<unsigned short>();
			unsigned short y = pBinReader->Read<unsigned short>();
			fontMetric.Width = pBinReader->Read<unsigned short>();
			fontMetric.Height = pBinReader->Read<unsigned short>();
			fontMetric.OffsetX = pBinReader->Read<short>();
			fontMetric.OffsetY = pBinReader->Read<short>();
			fontMetric.AdvanceX = pBinReader->Read<short>();
			fontMetric.Page = pBinReader->Read<unsigned char>();
			fontMetric.Channel = pBinReader->Read<unsigned char>();

			unsigned char count{ 0 };
			while (fontMetric.Channel != 1) // calculates which byte was set
			{
				++count;
				fontMetric.Channel = fontMetric.Channel >> 1;
			}
			fontMetric.Channel = count < 3 ? 2 - count : 3;

			fontMetric.TexCoord = DirectX::XMFLOAT2{ float(x) / pSpriteFont->m_TextureWidth, float(y) / pSpriteFont->m_TextureHeight };
			fontMetric.IsValid = true;
			
		}
		else
		{
			pBinReader->MoveBufferPosition(16);
		}
	}


	//DONE :)

	delete pBinReader;
	return pSpriteFont;
}

void SpriteFontLoader::Destroy(SpriteFont* objToDestroy)
{
	SafeDelete(objToDestroy);
}
