#include "stdafx.h"
#include "SandBoxScene.h"
#include "./../OverlordProject/Game/Components/EnemyManager.h"
#include "GameObject.h"
#include "./../OverlordProject/Game/Components/EnemyComponent.h"
#include "./../OverlordProject/Game/Components/TowerComponent.h"
#include "./../OverlordProject/Game/Components/BulletManager.h"
#include "./../OverlordProject/Game/Components/PlayerHealthComponent.h"
#include "./../OverlordProject/Game/Components/InputComponent.h"
#include "./../OverlordProject/Materials/Shadow/SkinnedDiffuseMaterial_Shadow.h"
#include "CubePrefab.h"
#include "./../OverlordProject/Game/Misc/GridLoader.h"
#include "DebugRenderer.h"
#include "PhysxProxy.h"
#include "TextRenderer.h"
#include "ContentManager.h"
#include "SpriteFont.h"
#include "../Components/TextComponent.h"
#include "../Components/Currency.h"
#include "../Components/GridComponent.h"
#include "../PostProcessing/AntiAliasing.h"
#include "SoundManager.h"
#include "FixedCamera.h"
#include "../OverlordProject/Materials/Shadow/DiffuseMaterial_Shadow.h"

SandBoxScene::SandBoxScene()
	: GameScene(L"SandBox")
	, m_pEnemyManager{nullptr}
	, m_pBulletManager{nullptr}
	, m_pPlayerHealth{nullptr}
	, m_pCurrency{}
	, m_pFont{nullptr}
	, m_pPauseMenuSprites{}
	, m_pAntiAliasing{nullptr}
	, m_IsAAActive{true}
	, m_IsPaused{false}
	, m_pBackgroundMusic{nullptr}
{
}

SandBoxScene::~SandBoxScene()
{
	if (!m_IsAAActive)
	{
		SafeDelete(m_pAntiAliasing);
	}
}

void SandBoxScene::Reset()
{
	m_IsPaused = false;
	m_pEnemyManager->Reset();
	m_pBulletManager->Reset();
	m_pPlayerHealth->ResetHealth(m_PlayerStartHealth);
	m_pCurrency->SetCurrency(m_StartCurrency);
	for (std::vector<GameObject*>& row : m_Grid.grid)
	{
		for (GameObject* pObj : row)
		{
			pObj->GetComponent<GridComponent>()->ClearTower();
		}
	}
}

void SandBoxScene::Initialize()
{
	GetGameContext().pShadowMapper->SetLight({ -100,210,-115}, { 0.5773502691896258f, -0.5773502691896258f, 0.5773502691896258f });

	// Init Textures
	SkinnedDiffuseMaterial_Shadow* pMat = new SkinnedDiffuseMaterial_Shadow{};
	GetGameContext().pMaterialManager->AddMaterial(pMat, 0);
	pMat->SetDiffuseTexture(L"Resources/Textures/EnemyDiffuse.png");

	DiffuseMaterial_Shadow* pBuildableMat = new DiffuseMaterial_Shadow();
	pBuildableMat->SetDiffuseTexture(L"Resources/Textures/BuildableTex.png");
	GetGameContext().pMaterialManager->AddMaterial(pBuildableMat, 2);

	DiffuseMaterial_Shadow* pRoadMat = new DiffuseMaterial_Shadow();
	pRoadMat->SetDiffuseTexture(L"Resources/Textures/RoadTex.png");
	GetGameContext().pMaterialManager->AddMaterial(pRoadMat, 1);


	DiffuseMaterial_Shadow* pNonBuilableMat = new DiffuseMaterial_Shadow();
	pNonBuilableMat->SetDiffuseTexture(L"Resources/Textures/NonBuildableTex.png");
	GetGameContext().pMaterialManager->AddMaterial(pNonBuilableMat, 3);


	m_pFont = ContentManager::Load<SpriteFont>(L"./Resources/SpriteFonts/Consolas_32.fnt");
	GameObject* pHealthObject = new GameObject();
	m_pPlayerHealth = new PlayerHealthComponent{ m_PlayerStartHealth };
	pHealthObject->AddComponent(m_pPlayerHealth);
	TextComponent* pLifeTextComponent = new TextComponent{ m_pFont, L"", {10,40} };
	pHealthObject->AddComponent(pLifeTextComponent);

	GameObject* pCurrencyObject = new GameObject();
	m_pCurrency = new Currency{};
	pCurrencyObject->AddComponent(m_pCurrency);
	TextComponent* pCurrencyText = new TextComponent{ m_pFont, L"", {10,15} };
	pCurrencyObject->AddComponent(pCurrencyText);
	AddChild(pCurrencyObject);

	m_Grid = LoadGrid(L"Resources/Grids/Grid.txt");
	for (std::vector<GameObject*>& row : m_Grid.grid)
	{
		for (GameObject* pObj : row)
		{
			AddChild(pObj);
		}
	}

	EnemyComponent::SetPath(m_Grid.wayPoints);
	m_pEnemyManager = new EnemyManager(0, m_pPlayerHealth, *m_pCurrency, m_IsPaused);
	GameObject* pEnemyManager = new GameObject();
	pEnemyManager->AddComponent(m_pEnemyManager);
	AddChild(pEnemyManager);

	m_pBulletManager = new BulletManager(50, m_IsPaused);
	GameObject* pBulletManager = new GameObject();
	pBulletManager->AddComponent(m_pBulletManager);
	AddChild(pBulletManager);

	// Create pauseMenu Sprites
	GameObject* pResumeButtonObject = new GameObject();
	SpriteComponent* pResumeButtonSprite = new SpriteComponent(L"Resources/Textures/ResumeButton.png", { 0.5f,0.5f });
	pResumeButtonObject->AddComponent(pResumeButtonSprite);
	pResumeButtonSprite->SetActive(false);
	AddChild(pResumeButtonObject);
	m_pPauseMenuSprites.push_back(pResumeButtonSprite);
	pResumeButtonObject->GetTransform()->Translate(650, 160, 0);

	GameObject* pResetButtonObject = new GameObject();
	SpriteComponent* pResetButton = new SpriteComponent(L"Resources/Textures/RestartButton.png", {0.5f,0.5f});
	pResetButtonObject->AddComponent(pResetButton);
	pResetButton->SetActive(false);
	AddChild(pResetButtonObject);
	m_pPauseMenuSprites.push_back(pResetButton);
	pResetButtonObject->GetTransform()->Translate(650, 340, 0);

	GameObject* pQuitButtonObject = new GameObject();
	SpriteComponent* pQuitButton = new SpriteComponent(L"Resources/Textures/QuitButton.png", {0.5f,0.5f});
	pQuitButtonObject->AddComponent(pQuitButton);
	pQuitButton->SetActive(false);
	AddChild(pQuitButtonObject);
	m_pPauseMenuSprites.push_back(pQuitButton);
	pQuitButtonObject->GetTransform()->Translate(650, 520, 0);

	// CreateInput
	InputComponent* pInput = new InputComponent(m_pEnemyManager, m_pBulletManager, *m_pCurrency, pResumeButtonObject, pResetButtonObject, pQuitButtonObject , m_IsPaused);
	pHealthObject->AddComponent(pInput);

	AddChild(pHealthObject);

	InputAction toggleAA{ 76, InputTriggerState::Pressed, 76 };
	GetGameContext().pInput->AddInputAction(toggleAA);

	InputAction togglePause{ 27, InputTriggerState::Pressed, 27 };
	GetGameContext().pInput->AddInputAction(togglePause);

	m_pCurrency->GainCurrency(m_StartCurrency);

	m_pAntiAliasing = new AntiAliasing();
	AddPostProcessingEffect(m_pAntiAliasing);

	FMOD_RESULT result = SoundManager::GetInstance()->GetSystem()->createSound(m_SoundFile, 0, nullptr, &m_pBackgroundMusic);
	if (result != FMOD_OK)
	{
		Logger::LogWarning(L"SandBoxScene::Initialize >> Failed to load background music");
	}
	else
	{
		m_pBackgroundMusic->setLoopCount(90000000);
		SoundManager::GetInstance()->GetSystem()->playSound(m_pBackgroundMusic, nullptr, false, nullptr);
	}

	FixedCamera* pCam = new FixedCamera();
	pCam->GetTransform()->Translate(100, 60, -25);

	pCam->GetTransform()->Rotate(DirectX::XMFLOAT3{ 45,0, 0 });
	AddChild(pCam);
	SetActiveCamera(pCam->GetComponent<CameraComponent>());


}

void SandBoxScene::Update()
{
	if (GetGameContext().pInput->IsActionTriggered(76))
	{
		if (m_IsAAActive)
		{
			RemovePostProcessingEffect(m_pAntiAliasing);
		}
		else
		{
			AddPostProcessingEffect(m_pAntiAliasing);
		}
		m_IsAAActive = !m_IsAAActive;
	}
	if (GetGameContext().pInput->IsActionTriggered(27))
	{
		m_IsPaused = !m_IsPaused;
	}
	for (SpriteComponent* pSpriteComponent : m_pPauseMenuSprites)
	{
		pSpriteComponent->SetActive(m_IsPaused);
	}
}

void SandBoxScene::Draw()
{
}

void SandBoxScene::ResetVariables()
{
}
