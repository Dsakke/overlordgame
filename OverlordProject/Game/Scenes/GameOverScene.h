#pragma once
#include "GameScene.h"

class GameOverScene final : public GameScene
{
public:
	GameOverScene();

private:
	void Initialize() override;
	void Update() override;
	void Draw() override;

	enum InputActions : int
	{
		MainMenu = 32
	};
};