#pragma once
#include "GameScene.h"
#include ".././Misc/GridLoader.h"
class Currency;
class EnemyManager;
class BulletManager;
class PlayerHealthComponent;
class SpriteFont;
class AntiAliasing;
class SpriteComponent;

class SandBoxScene final : public GameScene
{
public:
	SandBoxScene();

	SandBoxScene(const SandBoxScene&) = delete;
	SandBoxScene(SandBoxScene&&) = delete;
	SandBoxScene& operator=(const SandBoxScene&) = delete;
	SandBoxScene& operator=(SandBoxScene&&) = delete;
	~SandBoxScene();

	void Reset();
private:
	void Initialize() override;
	void Update() override;
	void Draw() override;

	void ResetVariables();

	EnemyManager* m_pEnemyManager;
	BulletManager* m_pBulletManager;
	PlayerHealthComponent* m_pPlayerHealth;
	Currency* m_pCurrency;
	SpriteFont* m_pFont;
	AntiAliasing* m_pAntiAliasing;
	std::vector<SpriteComponent*> m_pPauseMenuSprites;
	bool m_IsAAActive;
	bool m_IsPaused;
	Grid m_Grid;
	FMOD::Sound* m_pBackgroundMusic;
	const char* m_SoundFile = "Resources/Sounds/BackgroundMusic.mp3";
	// Default values
	int m_PlayerStartHealth = 10;
	int m_StartCurrency = 100;
};