#include "stdafx.h"
#include "GameOverScene.h"
#include "GameObject.h"
#include "TransformComponent.h"
#include "SceneManager.h"
#include "./../Components/TextComponent.h"
#include "ContentManager.h"
#include "SpriteFont.h"

GameOverScene::GameOverScene()
	: GameScene{L"GameOver"}
{
}

void GameOverScene::Initialize()
{
	SpriteFont* pFont = ContentManager::Load<SpriteFont>(L"./Resources/SpriteFonts/Consolas_32.fnt");

	GameObject* pGameOverObject = new GameObject();
	TextComponent* pGameOverText = new TextComponent(pFont, L"GAME OVER", { 575, 200 });
	pGameOverObject->AddComponent(pGameOverText);
	AddChild(pGameOverObject);

	GameObject* pInstructObject = new GameObject();
	TextComponent* pInstructText = new TextComponent(pFont, L"Press 'space' to go back to the main menu", { 350, 400 });
	pInstructObject->AddComponent(pInstructText);
	AddChild(pInstructObject);

	InputAction inputAction{ InputActions::MainMenu, InputTriggerState::Pressed, InputActions::MainMenu };
	GetGameContext().pInput->AddInputAction(inputAction);
}

void GameOverScene::Update()
{
	if (GetGameContext().pInput->IsActionTriggered(InputActions::MainMenu))
	{
		SceneManager::GetInstance()->SetActiveGameScene(L"MainMenu");
	}
}

void GameOverScene::Draw()
{
}
