#pragma once
#include "GameScene.h"

class MainMenuScene final : public GameScene
{
public:
	MainMenuScene();
	~MainMenuScene() = default;

private:
	void Initialize() override;
	void Update() override;
	void Draw() override;

	GameObject* m_pQuitButton;
	GameObject* m_pStartButton;
	std::vector<GameObject*> m_pPickableSprites;
};