#include "stdafx.h"
#include "MainMenuScene.h"
#include "GameObject.h"
#include "SpriteComponent.h"
#include "SceneManager.h"
#include "TransformComponent.h"

MainMenuScene::MainMenuScene()
	: GameScene(L"MainMenu")
	, m_pQuitButton{nullptr}
	, m_pStartButton{nullptr}
	, m_pPickableSprites{}
{
}

void MainMenuScene::Initialize()
{
	m_pQuitButton = new GameObject();
	SpriteComponent* pQuitSprite = new SpriteComponent(L"Resources/Textures/QuitButton.png");
	m_pQuitButton->GetTransform()->Translate(480, 300,0);
	m_pQuitButton->AddComponent(pQuitSprite);
	AddChild(m_pQuitButton);

	m_pStartButton = new GameObject();
	SpriteComponent* pStartSprite = new SpriteComponent(L"Resources/Textures/PlayButton.png");
	m_pStartButton->AddComponent(pStartSprite);
	AddChild(m_pStartButton);

	m_pPickableSprites.push_back(m_pQuitButton);
	m_pPickableSprites.push_back(m_pStartButton);

	InputAction clickAction{ 0, InputTriggerState::Pressed, -1, VK_LBUTTON };
	GetGameContext().pInput->AddInputAction(clickAction);
}

void MainMenuScene::Update()
{
	const GameContext& gameContext = GetGameContext();
	if (gameContext.pInput->IsActionTriggered(0))
	{
		GameObject* pPickedSprite = gameContext.pCamera->PickSprites(gameContext, m_pPickableSprites);
		if (pPickedSprite == m_pQuitButton)
		{
			PostQuitMessage(0);
		}
		else if (pPickedSprite == m_pStartButton)
		{
			SceneManager::GetInstance()->SetActiveGameScene(L"SandBox");
		}
	}
}

void MainMenuScene::Draw()
{
}
