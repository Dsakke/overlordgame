#include "stdafx.h"
#include "GridLoader.h"
#include <fstream>
#include <regex>
#include "./../Components/GridComponent.h"
#include "Factories.h"
#include <string>
#include "GameObject.h"
#include "TransformComponent.h"

Grid LoadGrid(const std::wstring& file)
{
	std::wfstream inStream{ file.c_str(), std::fstream::in };
	if (!inStream.is_open())
	{
		Logger::LogWarning(L"LoadGrid >> Failed to open GridFile");
	}
	// read contents of file
	std::wstring contents{};
	std::wstring temp{};
	while (!inStream.eof())
	{
		std::getline(inStream, temp);
		contents.append(temp);
	}

	// check if file contains grid block
	std::wregex gridBlockRegex{ L"\\{[\\r\\n]*(?:\\d,)+[\\r\\n]*\\}" };
	std::wsmatch gridBlock{};
	if (!std::regex_search(contents, gridBlock, gridBlockRegex))
	{
		Logger::LogWarning(L"LoadGrid >> No gridblock found in " + file);
		return Grid{};
	}

	// parse data in grid block
	Grid grid{};
	std::wstring gridBlockStr{ gridBlock[0] };
	std::wregex gridTypeRegex{ L"\\d" };
	std::wsregex_iterator itGrid(gridBlockStr.begin(), gridBlockStr.end(), gridTypeRegex);
	std::wsregex_iterator end;

	int yCoord = 0;
	grid.grid.push_back(std::vector<GameObject*>{});
	for (;itGrid != end; ++itGrid)
	{
		int gridType = std::stoi((*itGrid)[0]);
		if (gridType == 3)
		{
			++yCoord;
			grid.grid.push_back(std::vector<GameObject*>{});
		}
		else
		{
			switch (gridType)
			{
			case 0:
				grid.grid[yCoord].push_back(Factories::CreateNonBuildableGridComponent());
				break;
			case 1:
				grid.grid[yCoord].push_back(Factories::CreateBuildableGridComponent());
				break;
			case 2:
				grid.grid[yCoord].push_back(Factories::CreateRoadGridComponent());
			default:
				break;
			}
		}
	}
	// transform grid nodes to appropriate locations
	const float size{ 10 };
	for (size_t y{}; y < grid.grid.size(); ++y)
	{
		for (size_t x{}; x < grid.grid[y].size(); ++x)
		{
			grid.grid[y][x]->GetTransform()->Translate(x * size, 0, y * size);
		}
	}
	
	std::wregex wayPointBlockRegex{ L"\\[[\\r\\n]*(?:\\(\\d+,\\d+\\))*[\\r\\n]*\\]" };
	std::wsmatch wayPointBlock{};

	if (!std::regex_search(contents, wayPointBlock, wayPointBlockRegex))
	{
		Logger::LogWarning(L"LoadGrid >> No gridblock found in " + file);
		return Grid{};
	}

	std::wstring wayPointBlockStr{ wayPointBlock[0] };
	std::wregex wayPointRegex{L"\\((\\d+),(\\d+)\\)"};
	std::wsregex_iterator itWayPoint(wayPointBlockStr.begin(), wayPointBlockStr.end(), wayPointRegex);
	for (; itWayPoint != end; ++itWayPoint)
	{
		int x = std::stoi((*itWayPoint)[1]);
		int y = std::stoi((*itWayPoint)[2]);

		grid.wayPoints.push_back({ (x * size), 2, (y * size) });

	}


	return grid;
}
