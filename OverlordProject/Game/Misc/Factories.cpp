#include "stdafx.h"
#include "Factories.h"
#include "GameObject.h"
#include "./../OverlordProject/Game/Components/GridComponent.h"
#include "CubePrefab.h"
#include "PhysxManager.h"
#include "RigidBodyComponent.h"
#include "ColliderComponent.h"
#include "./../OverlordProject/Game/Components/TowerComponent.h"
#include "ModelComponent.h"

GameObject* Factories::CreateBuildableGridComponent()
{
    const DirectX::XMFLOAT3 cubeDim = { 10.f,7.f,10.f };
    physx::PxPhysics* pPhysics = PhysxManager::GetInstance()->GetPhysics();
    physx::PxMaterial* pBaseMat{ pPhysics->createMaterial(1.f,1.f,1.f) };
    std::shared_ptr<physx::PxGeometry> pGeom = std::make_shared<physx::PxBoxGeometry>(cubeDim.x / 2, cubeDim.y / 2, cubeDim.z / 2);

    GameObject* pObject = new GameObject{ };
    GridComponent* pGrid = new GridComponent{GridComponent::GridType::buildable};
    pObject->AddComponent(pGrid);
    pObject->GetTransform()->Scale(cubeDim);
    ModelComponent* pModel = new ModelComponent(L"Resources/Meshes/Cube.ovm");
    pObject->AddComponent(pModel);
    pModel->SetMaterial(2);
    
    RigidBodyComponent* pRB = new RigidBodyComponent(true);
    ColliderComponent* pCollider = new ColliderComponent{ pGeom, *pBaseMat };
    pObject->AddComponent(pRB);
    pObject->AddComponent(pCollider);
    pCollider->EnableTrigger(true);
    return pObject;
}

GameObject* Factories::CreateNonBuildableGridComponent()
{
    const DirectX::XMFLOAT3 cubeDim = { 10.f,6.f,10.f };
    physx::PxPhysics* pPhysics = PhysxManager::GetInstance()->GetPhysics();
    physx::PxMaterial* pBaseMat{ pPhysics->createMaterial(1.f,1.f,1.f) };
    std::shared_ptr<physx::PxGeometry> pGeom = std::make_shared<physx::PxBoxGeometry>(cubeDim.x / 2, cubeDim.y / 2, cubeDim.z / 2);


    GameObject* pObject = new GameObject{ };
    GridComponent* pGrid = new GridComponent{ GridComponent::GridType::nonBuildable };
    pObject->AddComponent(pGrid);

    pObject->GetTransform()->Scale(cubeDim);
    ModelComponent* pModel = new ModelComponent(L"Resources/Meshes/Cube.ovm");
    pObject->AddComponent(pModel);
    pModel->SetMaterial(3);

    RigidBodyComponent* pRB = new RigidBodyComponent(true);
    ColliderComponent* pCollider = new ColliderComponent{ pGeom, *pBaseMat };
    pObject->AddComponent(pRB);
    pObject->AddComponent(pCollider);
    pCollider->EnableTrigger(true);
    return pObject;
}

GameObject* Factories::CreateRoadGridComponent()
{
    const DirectX::XMFLOAT3 cubeDim = { 10.f,5.f,10.f };
    physx::PxPhysics* pPhysics = PhysxManager::GetInstance()->GetPhysics();
    physx::PxMaterial* pBaseMat{ pPhysics->createMaterial(1.f,1.f,1.f) };
    std::shared_ptr<physx::PxGeometry> pGeom = std::make_shared<physx::PxBoxGeometry>(cubeDim.x / 2, cubeDim.y / 2, cubeDim.z / 2);

    GameObject* pObject = new GameObject{ };
    pObject->GetTransform()->Scale(cubeDim);
    GridComponent* pGrid = new GridComponent{ GridComponent::GridType::nonBuildable };
    pObject->AddComponent(pGrid);
    ModelComponent* pModel = new ModelComponent(L"Resources/Meshes/Cube.ovm");
    pObject->AddComponent(pModel);
    pModel->SetMaterial(1);

    RigidBodyComponent* pRB = new RigidBodyComponent(true);
    ColliderComponent* pCollider = new ColliderComponent{ pGeom, *pBaseMat };
    pObject->AddComponent(pRB);
    pObject->AddComponent(pCollider);
    pCollider->EnableTrigger(true);
    return pObject;
}

GameObject* Factories::CreateTower(float radius, float attackSpeed, const BulletDesc& desc, EnemyManager* pEnemyManager, BulletManager* pBulletManager, const bool& isGamePaused)
{
    CubePrefab* pObject = new CubePrefab{ 5,5,5, {0.8f, 0.8f, 0.8f, 1.f} };
    TowerComponent* pTower = new TowerComponent{ radius, attackSpeed, desc, pEnemyManager, pBulletManager, isGamePaused };
    pObject->AddComponent(pTower);

    return pObject;
}
