#pragma once
class GameObject;
struct BulletDesc;
class EnemyManager;
class BulletManager;
namespace Factories
{
	GameObject* CreateBuildableGridComponent();
	GameObject* CreateNonBuildableGridComponent();
	GameObject* CreateRoadGridComponent();
	GameObject* CreateTower(float radius, float attackSpeed, const BulletDesc& desc, EnemyManager* pEnemyManager, BulletManager* pBulletManager, const bool& isGamePaused);
}