#include "stdafx.h"
#include "Subject.h"
#include <algorithm>
#include "Observer.h"


void Subject::AddObserver(Observer* pObserver)
{
	auto it = std::find(m_pObservers.begin(), m_pObservers.end(), pObserver);
	if (it == m_pObservers.end())
	{
		m_pObservers.push_back(pObserver);
	}
}

void Subject::RemoveObserver(Observer* pObserver)
{
	auto it = std::remove(m_pObservers.begin(), m_pObservers.end(), pObserver);
	m_pObservers.erase(it, m_pObservers.end());
}

void Subject::Notify(GameObject* pGameObject, Events event)
{
	auto func = [=](Observer* pObserver)
	{
		pObserver->OnNotify(pGameObject, event);
	};
	std::for_each(m_pObservers.begin(), m_pObservers.end(), func);
}
