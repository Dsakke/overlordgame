#pragma once
#include <vector>
#include "Events.h"

class Observer;
class GameObject;
class Subject
{
public:
	virtual ~Subject() = default;

	void AddObserver(Observer* pObserver);
	void RemoveObserver(Observer* pObserver);

protected:
	void Notify(GameObject* pGameObject, Events event);
private:
	std::vector<Observer*> m_pObservers; // Subject does not own these pointers
};
