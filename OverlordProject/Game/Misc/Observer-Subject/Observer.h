#pragma once
#include "Events.h"
class GameObject;
class Observer
{
public:
	virtual ~Observer() = default;
	virtual void OnNotify(GameObject* pObject, Events event) = 0;
private:
};