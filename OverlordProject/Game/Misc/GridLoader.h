#pragma once
#include <string>
#include <vector>
struct Grid
{
	std::vector<std::vector<GameObject*>> grid;
	std::vector<DirectX::XMFLOAT3> wayPoints;
};
Grid LoadGrid(const std::wstring& file); // Not a content loader because it doesn't need it to be managed