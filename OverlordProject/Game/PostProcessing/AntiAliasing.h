#pragma once
#include "PostProcessingMaterial.h"

struct ID3DX11EffectShaderResourceVariable;

class AntiAliasing final : public PostProcessingMaterial
{
public:
	AntiAliasing();
	AntiAliasing(const AntiAliasing&) = delete;
	AntiAliasing(AntiAliasing&&) = delete;
	AntiAliasing& operator=(const AntiAliasing&) = delete;
	AntiAliasing& operator=(AntiAliasing&&) = delete;
	~AntiAliasing();

private:
	void LoadEffectVariables() override;
	void UpdateEffectVariables(RenderTarget* pRendertarget) override;
	void ClearEffectVariables() override;

	ID3DX11EffectShaderResourceVariable* m_pTextureMapVariabele;
	ID3DX11EffectShaderResourceVariable* m_pDepthTextureMapVariable;
};