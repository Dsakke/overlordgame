#include "stdafx.h"
#include "AntiAliasing.h"
#include "RenderTarget.h"
#include "SceneManager.h"
#include "OverlordGame.h"


AntiAliasing::AntiAliasing()
	: PostProcessingMaterial(L"./Resources/Effects/Post/AntiAliasing.fx", 1)
	, m_pTextureMapVariabele(nullptr)
	, m_pDepthTextureMapVariable{nullptr}
{
}

void AntiAliasing::LoadEffectVariables()
{
	m_pTextureMapVariabele = GetEffect()->GetVariableByName("gTexture")->AsShaderResource();
	if (!m_pTextureMapVariabele->IsValid())
	{
		Logger::LogWarning(L"AntiAliasing::LoadEffectVariables >> Cannot find variable 'gTexture'");
	}
	m_pDepthTextureMapVariable = GetEffect()->GetVariableByName("gDepthBuffer")->AsShaderResource();
	if (!m_pDepthTextureMapVariable->IsValid())
	{
		Logger::LogWarning(L"AntiAliasing::LoadEffectVariables >> Cannot find variable 'gDepthBuffer'");
	}
}

void AntiAliasing::UpdateEffectVariables(RenderTarget* pRendertarget)
{
	m_pTextureMapVariabele->SetResource(pRendertarget->GetShaderResourceView());
	m_pDepthTextureMapVariable->SetResource(pRendertarget->GetDepthShaderResourceView());
}

void AntiAliasing::ClearEffectVariables()
{
	m_pTextureMapVariabele->SetResource(0);
	m_pDepthTextureMapVariable->SetResource(0);
}

AntiAliasing::~AntiAliasing()
{
}

