#include "stdafx.h"
#include "Currency.h"
#include "GameObject.h"
#include "TextComponent.h"
#include <string>

Currency::Currency()
	: m_Currency{0}
	, m_pTextComponent{nullptr}
{
}

bool Currency::SpendCurrency(int amount)
{
	if (m_Currency >= amount)
	{
		m_Currency -= amount;
		SetText();
		return true;
	}
	return false;
}

void Currency::GainCurrency(int amount)
{
	m_Currency += amount;
	SetText();
}

int Currency::GetCurrency() const
{
	return m_Currency;
}

void Currency::SetCurrency(int amount)
{
	m_Currency = amount;
	SetText();
}

void Currency::Initialize(const GameContext&)
{
	m_pTextComponent = GetGameObject()->GetComponent<TextComponent>();
	SetText();
}

void Currency::Update(const GameContext&)
{
}

void Currency::Draw(const GameContext&)
{
}

void Currency::SetText()
{
	m_pTextComponent->SetText(L"Currency: " + (m_Currency < 100 ? L"0" + std::to_wstring(m_Currency) : std::to_wstring(m_Currency)));
}
