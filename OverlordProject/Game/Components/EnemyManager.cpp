#include "stdafx.h"
#include "EnemyManager.h"
#include <algorithm>
#include "EnemyComponent.h"
#include "BaseComponent.h"
#include "TransformComponent.h"
#include "CubePrefab.h"
#include "GameScene.h"
#include "PlayerHealthComponent.h"
#include "Currency.h"
#include "ModelComponent.h"
#include "ParticleEmitterComponent.h"

EnemyManager::EnemyManager(int initSize, PlayerHealthComponent* pPlayerHealth, Currency& currency, const bool& isGamePaused)
	: m_Enemies{}
	, m_pPlayerHealth{pPlayerHealth}
	, m_NrEnemiesToSpawn{5}
	, m_Timer{0}
	, m_SpawnInterval{0.3f}
	, m_WaveInterval{20.f}
	, m_WaveNr{0}
	, m_Currency{currency}
	, m_IsGamePaused{isGamePaused}
{
	m_Enemies.resize(initSize);
}

void EnemyManager::AddEnemy(const EnemyDescription& desc, const GameContext& gameContext)
{
	auto predicate = [](GameObject* pObject)
	{
		EnemyComponent* pE =pObject->GetComponent<EnemyComponent>();
		return !pE->GetIsAlive();
	};
	auto it = std::find_if(m_Enemies.begin(), m_Enemies.end(), predicate);
	if (it == m_Enemies.end())
	{
		CreateNewEneny(desc);
	}
	else
	{
		(*it)->GetComponent<EnemyComponent>()->SetState(desc, gameContext);
	}

}

EnemyComponent* EnemyManager::GetTarget(const DirectX::XMFLOAT3& pos, float radius)
{
	using namespace DirectX;
	auto findTarget = [=](GameObject* pGameObject)
	{
		EnemyComponent* pE = pGameObject->GetComponent<EnemyComponent>();
		DirectX::XMVECTOR vecEnemyPos{ DirectX::XMLoadFloat3(&pGameObject->GetTransform()->GetPosition()) };
		DirectX::XMVECTOR vecTowerPos{ DirectX::XMLoadFloat3(&pos) };
		DirectX::XMVECTOR vec{ vecEnemyPos - vecTowerPos };
		DirectX::XMVECTOR magVec = DirectX::XMVector3Length(vec);
		DirectX::XMFLOAT3 mag{};
		DirectX::XMStoreFloat3(&mag, magVec);
		if (mag.x < radius && pE->GetIsAlive())
		{
			return true;
		}
		return false;
	};
	auto it = std::find_if(m_Enemies.begin(), m_Enemies.end(), findTarget);
	if (it != m_Enemies.end())
	{
		return (*it)->GetComponent<EnemyComponent>();
	}
	return nullptr;
}

void EnemyManager::OnNotify(GameObject* pGameObject, Events event)
{
	if (event == Events::EnemyDied)
	{
		EnemyComponent* pEnemy = pGameObject->GetComponent<EnemyComponent>();
		m_Currency.GainCurrency(pEnemy->GetKillReward());
	}
}

void EnemyManager::Reset()
{
	for (GameObject* pEnemy : m_Enemies)
	{
		EnemyComponent* pEnemyComponent = pEnemy->GetComponent<EnemyComponent>();
		pEnemyComponent->Damage(FLT_MAX);
	}
	m_WaveNr = 0;
	m_Timer = m_WaveInterval - 3.f;
	m_NrEnemiesToSpawn = 0;
}

void EnemyManager::Initialize(const GameContext&)
{
}

void EnemyManager::Update(const GameContext& gameContext)
{
	std::sort(m_Enemies.begin(), m_Enemies.end(), [](GameObject* pObject1, GameObject* pObject2) {return pObject1->GetComponent<EnemyComponent>()->GetTotalDistanceTravelled() > pObject2->GetComponent<EnemyComponent>()->GetTotalDistanceTravelled(); });
	m_Timer += gameContext.pGameTime->GetElapsed();
	if (m_NrEnemiesToSpawn > 0)
	{
		if (m_Timer >= m_SpawnInterval)
		{
			EnemyDescription desc{};
			desc.health = 100;
			desc.movementSpeed = 5.f;
			desc.MeshName = L"Resources/Meshes/Enemy.ovm";
			desc.modelScale = 0.05f;
			desc.killReward = 10;
			AddEnemy(desc, gameContext);
			--m_NrEnemiesToSpawn;
			m_Timer -= m_SpawnInterval;
		}
	}
	else if(m_Timer > m_WaveInterval)
	{
		CreateWave();
	}
}

void EnemyManager::Draw(const GameContext&)
{
}

void EnemyManager::CreateWave()
{
	m_NrEnemiesToSpawn = 5 + m_WaveNr;
	++m_WaveNr;
	m_Timer = 0.f;
}


void EnemyManager::CreateNewEneny(const EnemyDescription& desc)
{
	GameObject* pObject = new GameObject{};
	ModelComponent* pModel = new ModelComponent(desc.MeshName);
	pObject->AddComponent(pModel);
	pModel->SetMaterial(0);
	EnemyComponent* pEnemy = new EnemyComponent{ desc.movementSpeed, desc.health, m_pPlayerHealth, m_Currency, m_IsGamePaused };
	pObject->AddComponent(pEnemy);
	pEnemy->AddObserver(this);
	pObject->GetTransform()->Scale(desc.modelScale, desc.modelScale, desc.modelScale);
	ParticleEmitterComponent* pParticleEmiter = new ParticleEmitterComponent(L"Resources/Textures/Dust.png", 20);
	pParticleEmiter->SetMaxEmitterRange(0.0f);
	pParticleEmiter->SetMaxEnergy(1.f);
	pParticleEmiter->SetMinEnergy(0.75f);
	pParticleEmiter->SetMinSize(10.f);
	pParticleEmiter->SetMaxSize(15.f);
	pObject->AddComponent(pParticleEmiter);
	m_Enemies.push_back(pObject);
	GetGameObject()->GetScene()->AddChild(pObject);
}
