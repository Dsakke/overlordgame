#include "stdafx.h"
#include "TextComponent.h"
#include "TextRenderer.h" 

TextComponent::TextComponent(SpriteFont* pFont, const std::wstring& text, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT4& color)
	: m_pFont{pFont}
	, m_Text{text}
	, m_Position{pos}
	, m_Color{color}
{
}

void TextComponent::SetText(const std::wstring& text)
{
	m_Text = text;
}

void TextComponent::SetPosition(const DirectX::XMFLOAT2& pos)
{
	m_Position = pos;
}

void TextComponent::SetColor(const DirectX::XMFLOAT4& color)
{
	m_Color = color;
}

void TextComponent::SetFont(SpriteFont* pFont)
{
	m_pFont = pFont;
}

void TextComponent::Initialize(const GameContext& )
{
}

void TextComponent::Update(const GameContext& )
{
}

void TextComponent::Draw(const GameContext& )
{
	TextRenderer::GetInstance()->DrawText(m_pFont, m_Text, m_Position, m_Color);
}
