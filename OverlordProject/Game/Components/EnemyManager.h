#pragma once
#include "BaseComponent.h"
#include <vector>
#include "../OverlordProject/Game/Misc/Observer-Subject/Observer.h"
struct EnemyDescription
{
	float movementSpeed;
	float health;
	float modelScale;
	int killReward;
	std::wstring MeshName;
};
class EnemyComponent;
class PlayerHealthComponent;
class Currency;
class EnemyManager final : public BaseComponent, public Observer
{
public:
	EnemyManager(int initSize, PlayerHealthComponent* pPlayerHealth, Currency& currency, const bool& isGamePaused);

	EnemyManager(EnemyManager&& other) = delete;
	EnemyManager(const EnemyManager& other) = delete;
	EnemyManager& operator=(EnemyManager&& other) = delete;
	EnemyManager& operator=(const EnemyManager& other) = delete;

	void AddEnemy(const EnemyDescription& desc, const GameContext& gameContext);
	EnemyComponent* GetTarget(const DirectX::XMFLOAT3& pos, float radius);
	void OnNotify(GameObject* pGameObject, Events event) override;
	void Reset();

private:
	std::vector<GameObject*> m_Enemies;
	PlayerHealthComponent* m_pPlayerHealth;
	Currency& m_Currency;
	int m_NrEnemiesToSpawn;
	float m_Timer;
	const float m_SpawnInterval;
	float m_WaveInterval;
	int m_WaveNr;
	const bool& m_IsGamePaused;
	


	virtual void Initialize(const GameContext& gameContext) override;
	virtual void Update(const GameContext& gameContext) override;
	virtual void Draw(const GameContext& gameContext) override;
	void CreateWave();
	void CreateNewEneny(const EnemyDescription& desc);
};