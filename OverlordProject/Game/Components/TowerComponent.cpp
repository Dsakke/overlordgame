#include "stdafx.h"
#include "TowerComponent.h"
#include "EnemyComponent.h"
#include "EnemyManager.h"
#include "GameObject.h"
#include "GameScene.h"
#include "SoundManager.h"


TowerComponent::TowerComponent(float radius, float attackSpeed, const BulletDesc& desc, EnemyManager* pEnemyManager, BulletManager* pBulletManager, const bool& isGamePaused, const char* soundFile)
	: m_Radius{radius}
	, m_AttackSpeed{attackSpeed}
	, m_Timer{}
	, m_BulletDesc{desc}
	, m_pEnemyManager{pEnemyManager}
	, m_pBulletManager{pBulletManager}
	, m_pCurrentTarget{nullptr}
	, m_IsGamePaused{isGamePaused}
	, m_pShootSound{nullptr}
	, m_SoundFile{soundFile}
{
	m_BulletDesc.pTower = this;
}

void TowerComponent::Initialize(const GameContext&)
{
	if (strcmp(m_SoundFile,"") != 0)
	{
		FMOD_RESULT result = SoundManager::GetInstance()->GetSystem()->createSound(m_SoundFile, FMOD_DEFAULT, nullptr, &m_pShootSound);
		if (result != FMOD_RESULT::FMOD_OK)
		{
			Logger::LogWarning(L"TowerComponent::Initialize >> failed to load sound");
		}
	}
}

void TowerComponent::Update(const GameContext& gameContext)
{
	using namespace DirectX;
	if (m_IsGamePaused)
	{
		return;
	}

	m_pCurrentTarget = m_pEnemyManager->GetTarget(GetTransform()->GetPosition(), m_Radius);
	float dt = gameContext.pGameTime->GetElapsed();
	m_Timer += dt;
	if (m_pCurrentTarget)
	{
		DirectX::XMFLOAT3 pos = GetTransform()->GetPosition();
		DirectX::XMFLOAT3 enemyPos = m_pCurrentTarget->GetTransform()->GetPosition();
		// Set these on zero because we don't want to towers to rotate upwards or downwards
		pos.y = 0;
		enemyPos.y = 0;

		DirectX::XMVECTOR posVec = DirectX::XMLoadFloat3(&pos);
		DirectX::XMVECTOR enemyPosVec = DirectX::XMLoadFloat3(&enemyPos);

		DirectX::XMMATRIX lookAt = DirectX::XMMatrixLookAtRH(posVec, enemyPosVec, { 0,1,0 });
		DirectX::XMMATRIX lookAtInv = DirectX::XMMatrixInverse(nullptr, lookAt);
		DirectX::XMVECTOR rot = DirectX::XMQuaternionRotationMatrix(lookAtInv);
		GetTransform()->Rotate(rot);

		if (m_Timer > m_AttackSpeed)
		{
			m_Timer = 0;
			Shoot();
		}
	}
}

void TowerComponent::Draw(const GameContext&)
{
}

void TowerComponent::Shoot()
{
	m_BulletDesc.pEnemy = m_pCurrentTarget;
	m_pBulletManager->AddBullet(m_BulletDesc);
	SoundManager::GetInstance()->GetSystem()->playSound(m_pShootSound, nullptr, false, nullptr);
}
