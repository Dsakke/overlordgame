#include "stdafx.h"
#include "GridComponent.h"
#include "TowerComponent.h"
#include "GameObject.h"
#include "GameScene.h"
#include "SceneManager.h"

GridComponent::GridComponent(GridType type)
	: m_Type{type}
	, m_pTower{nullptr}
{
}

GridComponent::GridType GridComponent::GetType() const
{
	return m_Type;
}

TowerComponent* GridComponent::GetTower() const
{
	return m_pTower;
}

void GridComponent::SetTower(TowerComponent* pTower)
{
	m_pTower = pTower;
	GetGameObject()->GetScene()->AddChild(pTower->GetGameObject());
	DirectX::XMFLOAT3 pos = GetGameObject()->GetTransform()->GetPosition();
	pos.y += 7.f;
	pTower->GetGameObject()->GetTransform()->Translate(pos);
}

void GridComponent::ClearTower()
{
	if (!m_pTower)
	{
		return;
	}
	GameObject* pTowerObject = m_pTower->GetGameObject();
	SceneManager::GetInstance()->GetActiveScene()->RemoveChild(pTowerObject);
}

void GridComponent::Initialize(const GameContext& )
{
}

void GridComponent::Update(const GameContext& )
{
}

void GridComponent::Draw(const GameContext& )
{
}
