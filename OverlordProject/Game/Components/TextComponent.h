#pragma once
#include "BaseComponent.h"
#include <string>
class SpriteFont;
class TextComponent final : public BaseComponent
{
public:
	TextComponent(SpriteFont* pFont, const std::wstring& text = L"", const DirectX::XMFLOAT2& pos = { 0,0 }, const DirectX::XMFLOAT4& color = { 1,1,1,1 });
	TextComponent(const TextComponent&) = delete;
	TextComponent(TextComponent&&) = delete;
	TextComponent& operator=(const TextComponent&) = delete;
	TextComponent& operator=(TextComponent&&) = delete;

	void SetText(const std::wstring& text);
	void SetPosition(const DirectX::XMFLOAT2& pos);
	void SetColor(const DirectX::XMFLOAT4& color);
	void SetFont(SpriteFont* pFont);
private:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;

	SpriteFont* m_pFont;
	std::wstring m_Text;
	DirectX::XMFLOAT2 m_Position;
	DirectX::XMFLOAT4 m_Color;
};