#pragma once
#include "Components.h"
#include <functional>
#include "BulletManager.h"

namespace FMOD
{
	class Sound;
}
class EnemyManager;
class EnemyComponent;
class TowerComponent : public BaseComponent
{
public:
	TowerComponent(float radius, float attackSpeed, const BulletDesc& desc, EnemyManager* pEnemyManager, BulletManager* pBulletManager, const bool& isGamePaused, const char* soundFile = "Resources/Sounds/Shoot.wav");
private:
	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Draw(const GameContext& gameContext);
	void Shoot();

	float m_Radius;
	float m_AttackSpeed;
	float m_Timer;
	BulletDesc m_BulletDesc;
	EnemyManager* m_pEnemyManager; // also not owned by this object
	BulletManager* m_pBulletManager; // also not owned by this object
	EnemyComponent* m_pCurrentTarget; // not owned by this object
	const bool& m_IsGamePaused;
	FMOD::Sound* m_pShootSound;
	const char* m_SoundFile;

};