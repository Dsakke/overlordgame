#pragma once
#include "BaseComponent.h"
#include "GameObject.h"
#include <vector>
#include "../OverlordProject/Game/Misc/Observer-Subject/Subject.h"
struct EnemyDescription;
class PlayerHealthComponent;
class Currency;
class ParticleEmitterComponent;

class EnemyComponent final : public BaseComponent, public Subject
{
public:
	EnemyComponent(float ms, float health, PlayerHealthComponent* pPlayerHealth, Currency& currency, const bool& isGamePaused);
	EnemyComponent(PlayerHealthComponent* pPlayerHealth, Currency& currency, const bool& isGamePaused);
	
	void SetState(const EnemyDescription& desc, const GameContext& gameContext);

	float GetTotalDistanceTravelled() const;
	void Damage(float damage);
	bool GetIsAlive() const;
	static void SetPath(const std::vector<DirectX::XMFLOAT3>& path);
	int GetKillReward() const;
private:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;
	void ReachedEnd();
	void Died();

	float m_MovementSpeed;
	float m_Health;
	float m_DistanceTravelled;
	int m_KillReward;
	size_t m_PathIndex;
	static std::vector<DirectX::XMFLOAT3> m_Path;
	PlayerHealthComponent* m_pPlayerHealth;
	Currency& m_Currency;
	bool m_IsAlive;
	const bool& m_IsGamePaused;
	ParticleEmitterComponent* m_pParticleEmitter;
	float m_RunDustVelocity = 10.f;
};