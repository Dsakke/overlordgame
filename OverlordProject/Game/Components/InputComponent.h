#pragma once
#include "BaseComponent.h"

class Currency;
class EnemyManager;
class BulletManager;
class InputComponent final : public BaseComponent
{
public:
	InputComponent(EnemyManager* pEnemyManager, BulletManager* pBulletManager, Currency& currency, GameObject* pResumeButton, GameObject* pRestartButton, GameObject* pQuitButton, bool& m_IsGamePaused);
	~InputComponent() = default;
private:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;

	void Pick(const GameContext& gameContext) const;

	EnemyManager* m_pEnemyManager; // needed to init towers
	Currency& m_Currency;
	BulletManager* m_pBulletManager;
	std::vector<GameObject*> m_pPauseMenuPickableSprites;
	GameObject* m_pResumeButton;
	GameObject* m_pRestartButton;
	GameObject* m_pQuitButton;
	bool& m_IsGamePaused;
};