#pragma once
#include "BaseComponent.h"
#include "BulletManager.h"

class TowerComponent;
class EnemyComponent;
class BulletComponent : public BaseComponent
{
public:
	BulletComponent(TowerComponent* pTower, EnemyComponent* pEnemy, float velocity, float damage, const bool& isPaused);
	BulletComponent(const bool& isPaused);
	virtual ~BulletComponent() = default;
	bool GetIsActive() const;
	void SetState(const BulletDesc& bulletDesc);
	void Deactivate();
private:
	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Draw(const GameContext& gameContext);

	void EnemyHit();

	TowerComponent* m_pTower;
	EnemyComponent* m_pEnemy;
	float m_Velocity;
	float m_Damage;
	bool m_IsActive;
	const bool& m_IsPaused;
};