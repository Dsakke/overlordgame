#include "stdafx.h"
#include "BulletComponent.h"
#include "TowerComponent.h"
#include "EnemyComponent.h"
#include "TransformComponent.h"
BulletComponent::BulletComponent(TowerComponent* pTower, EnemyComponent* pEnemy, float velocity, float damage, const bool& isPaused)
	: m_pTower{pTower}
	, m_pEnemy{pEnemy}
	, m_Velocity{velocity}
	, m_Damage{damage}
	, m_IsActive{true}
	, m_IsPaused{isPaused}
{
	
}

BulletComponent::BulletComponent(const bool& isPaused)
	: m_pTower{nullptr}
	, m_pEnemy{nullptr}
	, m_Velocity{}
	, m_Damage{}
	, m_IsActive{false}
	, m_IsPaused{isPaused}
{

}

bool BulletComponent::GetIsActive() const
{
	return m_IsActive;
}

void BulletComponent::SetState(const BulletDesc& bulletDesc)
{
	m_pTower = bulletDesc.pTower;
	m_pEnemy = bulletDesc.pEnemy;
	m_Velocity = bulletDesc.velocity;
	m_Damage = bulletDesc.damage;

	GetTransform()->Translate(bulletDesc.pTower->GetTransform()->GetPosition());
	m_IsActive = true;
}

void BulletComponent::Deactivate()
{
	GetTransform()->Translate(9000, 9000, 9000);
	m_IsActive = false;
}



void BulletComponent::Initialize(const GameContext& )
{
	if (m_pTower)
	{
		GetTransform()->Translate(m_pTower->GetTransform()->GetPosition());
	}
}

void BulletComponent::Update(const GameContext& gameContext)
{
	if (!m_IsActive || m_IsPaused)
	{
		return;
	}
	if (!m_pEnemy->GetIsAlive())
	{
		m_IsActive = false;
		GetTransform()->Translate(9000, 9000, 9000);
	}
	using namespace DirectX;
	// Load in positions
	const DirectX::XMFLOAT3& currentPos = GetTransform()->GetPosition();
	const DirectX::XMFLOAT3& enemyPos = m_pEnemy->GetTransform()->GetPosition();
	DirectX::XMVECTOR currentPosVec = DirectX::XMLoadFloat3(&currentPos);
	DirectX::XMVECTOR enemyPosVec = DirectX::XMLoadFloat3(&enemyPos);

	// Get the vector between the 2 positions
	DirectX::XMVECTOR travelDir = enemyPosVec - currentPosVec;

	// Calculate magnitude and check if within range of enemy position
	DirectX::XMVECTOR magnitudeVec = DirectX::XMVector3Length(travelDir);
	DirectX::XMFLOAT3 magnitude{};
	DirectX::XMStoreFloat3(&magnitude, magnitudeVec);
	if (magnitude.x <= 0.5f)
	{
		EnemyHit();
	}

	// Normalize the vector between the points and multiply with velocity and dt
	travelDir = DirectX::XMVector3Normalize(travelDir);
	DirectX::XMVECTOR newPosVec = currentPosVec + travelDir * m_Velocity * gameContext.pGameTime->GetElapsed();
	GetTransform()->Translate(newPosVec);
	
}

void BulletComponent::Draw(const GameContext& )
{
}

void BulletComponent::EnemyHit()
{
	GetTransform()->Translate(9000, 9000, 9000);
	m_pEnemy->Damage(m_Damage);
	m_IsActive = false;
}
