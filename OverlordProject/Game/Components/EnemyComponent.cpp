#include "stdafx.h"
#include "EnemyComponent.h"
#include "BulletComponent.h"
#include "GameScene.h"
#include "ModelComponent.h"
#include "EnemyManager.h"
#include "TransformComponent.h"
#include "PlayerHealthComponent.h"
#include "ParticleEmitterComponent.h"

std::vector<DirectX::XMFLOAT3> EnemyComponent::m_Path{};

EnemyComponent::EnemyComponent(float ms, float health, PlayerHealthComponent* pPlayerHealth, Currency& currency, const bool& isGamePaused)
	: m_MovementSpeed{ms}
	, m_Health{health}
	, m_DistanceTravelled{0}
	, m_IsAlive{true}
	, m_PathIndex{ 1 }
	, m_pPlayerHealth{pPlayerHealth}
	, m_Currency{currency}
	, m_KillReward{0}
	, m_IsGamePaused{isGamePaused}
	, m_pParticleEmitter{}
{
}

EnemyComponent::EnemyComponent(PlayerHealthComponent* pPlayerHealth, Currency& currency, const bool& isGamePaused)
	: m_MovementSpeed{0}
	, m_Health{0}
	, m_DistanceTravelled{0}
	, m_IsAlive{false}
	, m_PathIndex{1}
	, m_pPlayerHealth{ pPlayerHealth }
	, m_KillReward{0}
	, m_Currency{currency}
	, m_IsGamePaused{isGamePaused}
	, m_pParticleEmitter{}
{
}

void EnemyComponent::SetState(const EnemyDescription& desc, const GameContext& gameContext)
{
	if (m_IsAlive)
	{
		Logger::LogWarning(L"EnemyComponent::SetState >> Tried to set state on an enemy that was still alive");
		return;
	}
	m_MovementSpeed = desc.movementSpeed;
	m_Health = desc.health;
	m_DistanceTravelled = 0;
	m_PathIndex = 1;
	m_KillReward = desc.killReward;
	GetGameObject()->GetTransform()->Translate(m_Path[0]);
	m_IsAlive = true;
	ModelComponent* pModel = GetGameObject()->GetComponent<ModelComponent>();
	pModel->SetMaterial(0);
	GetTransform()->Scale(desc.modelScale, desc.modelScale, desc.modelScale);
	if (pModel)
	{
		pModel->ChangeMesh(desc.MeshName, gameContext);
	}
	else
	{
		Logger::LogWarning(L"EnemyComponent::SetState >> No model component was attached to the Enemy");
	}
}

float EnemyComponent::GetTotalDistanceTravelled() const
{
	return m_DistanceTravelled;
}

void EnemyComponent::Damage(float damage)
{
	if (!m_IsAlive)
		return;

	m_Health -= damage;
	if (m_Health <= 0.0f)
	{
		Died();
	}
}

bool EnemyComponent::GetIsAlive() const
{
	return m_IsAlive;
}

void EnemyComponent::SetPath(const std::vector<DirectX::XMFLOAT3>& path)
{
	m_Path = path;
}

int EnemyComponent::GetKillReward() const
{
	return m_KillReward;
}

void EnemyComponent::Initialize(const GameContext&)
{
	GetTransform()->Translate(m_Path[0]);
	m_pParticleEmitter = GetGameObject()->GetComponent<ParticleEmitterComponent>();
}

void EnemyComponent::Update(const GameContext& gameContext)
{
	if (!m_IsAlive || m_IsGamePaused)
	{
		return;
	}

	if (m_Path.empty())
	{
		Logger::LogWarning(L"EnemyComponent::Update >> Enemy was alive with no path set");
		return;
	}

	using namespace DirectX;
	// Load Positions
	const DirectX::XMFLOAT3& currentPos = GetTransform()->GetPosition();
	const DirectX::XMFLOAT3& nextNode = m_Path[m_PathIndex];
	DirectX::XMVECTOR currentPosVec = DirectX::XMLoadFloat3(&currentPos);
	DirectX::XMVECTOR nextNodeVec = DirectX::XMLoadFloat3(&nextNode);

	// check if enemy if close to next node and calculate direction to travel in
	DirectX::XMVECTOR dirVec = nextNodeVec - currentPosVec;
	DirectX::XMVECTOR magVec = DirectX::XMVector3Length(dirVec);
	DirectX::XMFLOAT3 mag{};
	DirectX::XMStoreFloat3(&mag, magVec);
	if (mag.x < 0.1f)
	{
		++m_PathIndex;
		// check if enemy reached end of the path
		if (m_PathIndex == m_Path.size())
		{
			ReachedEnd();
			return;
		}
		// recalculate direction
		nextNodeVec = DirectX::XMLoadFloat3(&m_Path[m_PathIndex]);
		dirVec = nextNodeVec - currentPosVec;
	}
	TransformComponent* pTransform = GetTransform();

	// Make the enemy face the right direction
	DirectX::XMMATRIX lookAt = DirectX::XMMatrixLookAtRH(currentPosVec, nextNodeVec, { 0,1,0 });
	DirectX::XMMATRIX lookAtInv = DirectX::XMMatrixInverse(nullptr, lookAt);
	DirectX::XMVECTOR rot = DirectX::XMQuaternionRotationMatrix(lookAtInv);
	pTransform->Rotate(rot);

	// Calculate new position
	dirVec = DirectX::XMVector3Normalize(dirVec);
	float distanceTravelled = m_MovementSpeed * gameContext.pGameTime->GetElapsed();
	m_DistanceTravelled += distanceTravelled;
	dirVec *= distanceTravelled;
	DirectX::XMVECTOR newPosVec = currentPosVec + dirVec;
	pTransform->Translate(newPosVec);

	if (m_pParticleEmitter)
	{
		DirectX::XMFLOAT3 dir{};
		DirectX::XMStoreFloat3(&dir, -(dirVec * m_RunDustVelocity));
		m_pParticleEmitter->SetVelocity(dir);
	}
}

void EnemyComponent::Draw(const GameContext&)
{
}

void EnemyComponent::ReachedEnd()
{
	Logger::Log(LogLevel::Info, L"Enemy reached end");
	GetTransform()->Translate(9000, 9000, 9000);
	m_IsAlive = false;
	m_pPlayerHealth->TakeDamage(1);
}

void EnemyComponent::Died()
{
	Notify(GetGameObject(), Events::EnemyDied);
	m_IsAlive = false;
	GetTransform()->Translate( DirectX::XMFLOAT3{ 9000, 9000, 9000 } );
}


