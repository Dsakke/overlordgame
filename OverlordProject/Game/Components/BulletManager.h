#pragma once
#include "BaseComponent.h"
#include <vector>
class BulletComponent;
class TowerComponent;
class EnemyComponent;

struct BulletDesc
{
	TowerComponent* pTower;
	EnemyComponent* pEnemy;
	float velocity;
	float damage;
};

class BulletManager : public BaseComponent
{
public:
	BulletManager(int initSize, const bool& isPaused);
	void AddBullet(const BulletDesc& desc);
	void Reset();
private:
	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Draw(const GameContext& gameContext);
	BulletComponent* MakeBullet();


	std::vector<BulletComponent*> m_Bullets;
	const bool& m_IsPaused;
};