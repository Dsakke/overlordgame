#pragma once
#include "BaseComponent.h"
class TextComponent;
class PlayerHealthComponent final : public BaseComponent
{
public:
	PlayerHealthComponent(int maxHealth);
	void TakeDamage(int amount);
	void ResetHealth(int amount);
private:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;

	void EndGame();

	int m_MaxHealth;
	int m_CurrentHealth;
	TextComponent* m_pTextComponent;
};