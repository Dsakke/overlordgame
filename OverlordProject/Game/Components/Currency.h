#pragma once
#include "BaseComponent.h"
class TextComponent;
class Currency final : public BaseComponent
{
public:
	Currency();
	~Currency() = default;
	Currency(const Currency&) = delete;
	Currency(Currency&&) = delete;
	Currency& operator=(const Currency&) = delete;
	Currency& operator=(Currency&&) = delete;

	bool SpendCurrency(int amount);
	void GainCurrency(int amount);
	int GetCurrency() const;
	void SetCurrency(int amount);
private:
	virtual void Initialize(const GameContext& gameContext) override;
	virtual void Update(const GameContext& gameContext) override;
	virtual void Draw(const GameContext& gameContext) override;


	void SetText();

	int m_Currency;
	TextComponent* m_pTextComponent;

};
