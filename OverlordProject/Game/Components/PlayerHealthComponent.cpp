#include "stdafx.h"
#include "PlayerHealthComponent.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "TextComponent.h"
#include ".././Scenes/SandBoxScene.h"

PlayerHealthComponent::PlayerHealthComponent(int maxHealth)
	: m_MaxHealth{ maxHealth }
	, m_CurrentHealth{ maxHealth }
	, m_pTextComponent{ nullptr }
{
}

void PlayerHealthComponent::TakeDamage(int amount)
{
	m_CurrentHealth -= amount;
	if (m_CurrentHealth <= 0)
	{
		EndGame();
	}
	m_pTextComponent->SetText(L"Lives: " + std::to_wstring(m_CurrentHealth));

	m_pTextComponent->SetText(L"Lives: " + (m_CurrentHealth < 10 ? L"0" + std::to_wstring(m_CurrentHealth) : std::to_wstring(m_CurrentHealth)));
}

void PlayerHealthComponent::ResetHealth(int amount)
{
	m_MaxHealth = amount;
	m_CurrentHealth = amount;
}

void PlayerHealthComponent::Initialize(const GameContext&)
{
	m_pTextComponent = GetGameObject()->GetComponent<TextComponent>();
	m_pTextComponent->SetText(L"Lives: " + std::to_wstring(m_CurrentHealth));
}

void PlayerHealthComponent::Update(const GameContext&)
{
}

void PlayerHealthComponent::Draw(const GameContext&)
{
}

void PlayerHealthComponent::EndGame()
{
	SandBoxScene* pScene = dynamic_cast<SandBoxScene*>(SceneManager::GetInstance()->GetActiveScene());
	pScene->Reset();
	SceneManager::GetInstance()->SetActiveGameScene(L"GameOver");
}
