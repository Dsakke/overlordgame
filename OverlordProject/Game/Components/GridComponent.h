#pragma once
#include <BaseComponent.h>
class TowerComponent;
class GridComponent final : public BaseComponent
{
public:
	enum class GridType
	{
		nonBuildable = 0,
		buildable = 1,
		road = 2
	};

	GridComponent(GridType type);
	~GridComponent() = default;

	GridType GetType() const;
	TowerComponent* GetTower() const;
	void SetTower (TowerComponent* pTower);
	void ClearTower();

private:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;
	GridType m_Type;
	TowerComponent* m_pTower;
};

