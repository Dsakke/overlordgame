#include "stdafx.h"
#include "InputComponent.h"
#include "GameObject.h"
#include "GridComponent.h"
#include "GameScene.h"
#include "TowerComponent.h"
#include "./../Misc/Factories.h"
#include "Currency.h"
#include "SceneManager.h"
#include "./../Scenes/SandBoxScene.h"

InputComponent::InputComponent(EnemyManager* pEnemyManager, BulletManager* pBulletManager, Currency& currency, GameObject* pResumeButton, GameObject* pRestartButton, GameObject* pQuitButton, bool& isGamePaused)
	: m_pEnemyManager{pEnemyManager}
	, m_Currency{currency}
	, m_pBulletManager{pBulletManager}
	, m_pPauseMenuPickableSprites{}
	, m_pResumeButton{pResumeButton}
	, m_pRestartButton{pRestartButton}
	, m_pQuitButton{pQuitButton}
	, m_IsGamePaused{isGamePaused}
{
}

void InputComponent::Initialize(const GameContext& gameContext)
{
	InputAction pickAction{ 1, InputTriggerState::Pressed, -1, VK_LBUTTON };
	gameContext.pInput->AddInputAction(pickAction);

	m_pPauseMenuPickableSprites.push_back(m_pResumeButton);
	m_pPauseMenuPickableSprites.push_back(m_pRestartButton);
	m_pPauseMenuPickableSprites.push_back(m_pQuitButton);
}

void InputComponent::Update(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(1))
	{
		if (m_IsGamePaused)
		{
			GameObject* pPickedSprite = gameContext.pCamera->PickSprites(gameContext, m_pPauseMenuPickableSprites);
			if (pPickedSprite)
			{
				if (pPickedSprite == m_pResumeButton)
				{
					m_IsGamePaused = !m_IsGamePaused;
				}
				else if (pPickedSprite == m_pRestartButton)
				{
					SandBoxScene* pScene = dynamic_cast<SandBoxScene*>(SceneManager::GetInstance()->GetActiveScene());
					if (pScene)
					{
						pScene->Reset();
					}
				}
				else if (pPickedSprite == m_pQuitButton)
				{
					PostQuitMessage(0);
				}
			}
		}
		else
		{
			Pick(gameContext);
		}
	}

}

void InputComponent::Draw(const GameContext& )
{
}

void InputComponent::Pick(const GameContext& gameContext) const
{
	GameObject* pObject = gameContext.pCamera->Pick(gameContext);
	if (!pObject)
	{
		return;
	}

	GridComponent* pGridComp = pObject->GetComponent<GridComponent>();
	if (pGridComp)
	{
		if (pGridComp->GetType() == GridComponent::GridType::buildable)
		{
			TowerComponent* pTower = pGridComp->GetTower();
			if (!pTower && m_Currency.SpendCurrency(20))
			{
				BulletDesc desc{};
				desc.damage = 10.f;
				desc.velocity = 20.f;
				GameObject* pObj = Factories::CreateTower(100, 1, desc, m_pEnemyManager, m_pBulletManager, m_IsGamePaused);
				pGridComp->SetTower(pObj->GetComponent<TowerComponent>());
			}
		}
	}

}
