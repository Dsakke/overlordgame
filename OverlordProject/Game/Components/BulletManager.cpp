#include "stdafx.h"
#include "BulletManager.h"
#include <algorithm>
#include "BulletComponent.h"
#include "GameObject.h"
#include "RigidBodyComponent.h"
#include "ColliderComponent.h"
#include "GameScene.h"
#include "PhysxManager.h"
#include "SpherePrefab.h"
#include "TransformComponent.h"

BulletManager::BulletManager(int initSize, const bool& isPaused)
	: m_Bullets{}
	, m_IsPaused{ isPaused }
{
	m_Bullets.resize(initSize);

}

void BulletManager::AddBullet(const BulletDesc& desc)
{
	auto pred = [](const BulletComponent* pBullet) { return !pBullet->GetIsActive(); };
	auto it = std::find_if(m_Bullets.begin(), m_Bullets.end(), pred);
	if (it != m_Bullets.end())
	{
		(*it)->SetState(desc);
	}
	else
	{
		physx::PxPhysics* pPhysx{ PhysxManager::GetInstance()->GetPhysics() };
		physx::PxMaterial* pMat{ pPhysx->createMaterial(1.f,1.f,1.f) };
		GameObject* pObject = new GameObject();
		BulletComponent* pBullet = new BulletComponent(m_IsPaused);
		pBullet->SetState(desc);
		std::shared_ptr<physx::PxGeometry> pGeom = std::make_shared<physx::PxSphereGeometry>(1.f);
		
		RigidBodyComponent* pRB = new RigidBodyComponent{};
		ColliderComponent* pComponent = new ColliderComponent{ pGeom, *pMat };
		pRB->SetKinematic(true);
		pObject->AddComponent(pRB);
		pObject->AddComponent(pComponent);
	}
}

void BulletManager::Reset()
{
	for (BulletComponent* pBullet : m_Bullets)
	{
		pBullet->Deactivate();
	}
}

void BulletManager::Initialize(const GameContext&)
{
	for (size_t i{}; i < m_Bullets.size(); ++i)
	{
		m_Bullets[i] = MakeBullet();
	}
}

void BulletManager::Update(const GameContext&)
{
}

void BulletManager::Draw(const GameContext&)
{
}

BulletComponent* BulletManager::MakeBullet()
{
	float rad = 1.f;
	physx::PxPhysics* pPhysx{ PhysxManager::GetInstance()->GetPhysics() };
	physx::PxMaterial* pMat{ pPhysx->createMaterial(1.f,1.f,1.f) };

	BulletComponent* pBullet = new BulletComponent{m_IsPaused};
	SpherePrefab* pObject = new SpherePrefab(rad, 10, DirectX::XMFLOAT4{ 1,1,1,1 });
	pObject->AddComponent(pBullet);
	std::shared_ptr<physx::PxGeometry> pGeom{ std::make_shared<physx::PxSphereGeometry>(rad) };

	ColliderComponent* pCollider = new ColliderComponent{ pGeom, *pMat };
	pObject->AddComponent(pCollider);

	RigidBodyComponent* pRB = new RigidBodyComponent();
	pRB->SetKinematic(true);
	pObject->AddComponent(pRB);

	GetGameObject()->GetScene()->AddChild(pObject);
	pObject->GetTransform()->Translate(9000, 9000, 9000);
	return pBullet;
}
