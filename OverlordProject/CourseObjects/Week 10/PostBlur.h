#pragma once
#include "PostProcessingMaterial.h"

struct ID3DX11EffectShaderResourceVariable;

class PostBlur : public PostProcessingMaterial
{
public:
	PostBlur();
	virtual ~PostBlur() = default;

	PostBlur(const PostBlur& other) = delete;
	PostBlur(PostBlur&& other) noexcept = delete;
	PostBlur& operator=(const PostBlur& other) = delete;
	PostBlur& operator=(PostBlur&& other) noexcept = delete;
protected:
	void LoadEffectVariables() override;
	void UpdateEffectVariables(RenderTarget* pRendertarget) override;
	void ClearEffectVariables() override;
private:
	ID3DX11EffectShaderResourceVariable* m_pTextureMapVariabele;
};
