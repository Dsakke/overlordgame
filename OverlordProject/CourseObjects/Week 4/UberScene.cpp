#include "stdafx.h"
#include "UberScene.h"
#include "GameObject.h"
#include "ModelComponent.h"
#include "../OverlordProject/CourseObjects/Week 3/UberMaterial.h"

UberScene::UberScene()
	: GameScene{ L"UberScene" }
	, m_pTeapot{nullptr}
{
}

UberScene::~UberScene()
{
}

void UberScene::Initialize()
{
	m_pTeapot = new GameObject{};

	ModelComponent* pModel = new ModelComponent{ L"Resources/Meshes/Teapot.ovm" };
	UberMaterial* pUberMat = new UberMaterial{};
	pUberMat->EnableDiffuseTexture(true);
	pUberMat->SetDiffuseTexture(L"Resources/Textures/Chair_Dark.dds");
	pUberMat->EnableSpecularPhong(true);
	pUberMat->EnableEnvironmentMapping(true);
	pUberMat->SetEnvironmentCube(L"Resources/Textures/Sunol_Cubemap.dds");
	pUberMat->SetReflectionStrength(0.5f);
	GetGameContext().pMaterialManager->AddMaterial(pUberMat, 0);
	pModel->SetMaterial(0);
	m_pTeapot->AddComponent(pModel);
	AddChild(m_pTeapot);
}

void UberScene::Update()
{
}

void UberScene::Draw()
{
}
