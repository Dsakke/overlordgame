#pragma once
#include "GameScene.h"

class UberScene final : public GameScene
{
public:
	UberScene();
	~UberScene();

	UberScene(const UberScene&) = delete;
	UberScene(UberScene&&) = delete;
	UberScene& operator=(const UberScene&) = delete;
	UberScene& operator=(UberScene&&) = delete;

private:
	void Initialize() override;
	void Update() override;
	void Draw() override;

	GameObject* m_pTeapot;
};