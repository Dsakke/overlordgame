
#include "stdafx.h"
#include "Character.h"
#include "Components.h"
#include "Prefabs.h"
#include "GameScene.h"
#include "PhysxManager.h"
#include "PhysxProxy.h"


Character::Character(float radius, float height, float moveSpeed) : 
	m_Radius(radius),
	m_Height(height),
	m_MoveSpeed(moveSpeed),
	m_pCamera(nullptr),
	m_pController(nullptr),
	m_TotalPitch(0), 
	m_TotalYaw(0),
	m_RotationSpeed(90.f),
	//Running
	m_MaxRunVelocity(50.0f), 
	m_TerminalVelocity(20),
	m_Gravity(9.81f), 
	m_RunAccelerationTime(0.3f), 
	m_JumpAccelerationTime(0.8f), 
	m_RunAcceleration(m_MaxRunVelocity/m_RunAccelerationTime), 
	m_JumpAcceleration(m_Gravity/m_JumpAccelerationTime), 
	m_RunVelocity(0), 
	m_JumpVelocity(0),
	m_Velocity(0,0,0)
{}

void Character::Initialize(const GameContext& gameContext)
{
	physx::PxMaterial* pMat = GetScene()->GetPhysxProxy()->GetPhysxScene()->getPhysics().createMaterial(0.5f, 0.5f, 0.5f);
	//TODO: Create controller
	m_pController = new ControllerComponent(pMat, m_Radius, m_Height, L"Character", physx::PxCapsuleClimbingMode::eEASY);
	AddComponent(m_pController);
	//TODO: Add a fixed camera as child
	GameObject* pCam = new FixedCamera{};
	AddChild(pCam);
	m_pCamera = pCam->GetComponent<CameraComponent>();

	//TODO: Register all Input Actions
	InputAction leftAction{ LEFT, InputTriggerState::Down, 37 };
	gameContext.pInput->AddInputAction(leftAction);

	InputAction rightAction{ RIGHT, InputTriggerState::Down, 39 };
	gameContext.pInput->AddInputAction(rightAction);

	InputAction forwardAction{ FORWARD, InputTriggerState::Down, 38 };
	gameContext.pInput->AddInputAction(forwardAction);

	InputAction backwardAction{ BACKWARD, InputTriggerState::Down, 40 };
	gameContext.pInput->AddInputAction(backwardAction);

	InputAction jumpAction{ JUMP, InputTriggerState::Pressed, 32 };
	gameContext.pInput->AddInputAction(jumpAction);

	
}

void Character::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//TODO: Set the camera as active
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
	GetScene()->SetActiveCamera(m_pCamera);
}

void Character::Update(const GameContext& gameContext)
{
	using namespace DirectX;
	//TODO: Update the character (Camera rotation, Character Movement, Character Gravity)
	TransformComponent* pTransform = GetTransform(); 
	DirectX::XMFLOAT3 pos = pTransform->GetPosition();
	InputManager* pInput = gameContext.pInput;
	float dt = gameContext.pGameTime->GetElapsed();
	DirectX::XMFLOAT3 move{};

	if (pInput->IsActionTriggered(LEFT))
	{
		move.z -= 1;
	}
	if (pInput->IsActionTriggered(RIGHT))
	{
		move.z += 1;
	}
	if (pInput->IsActionTriggered(FORWARD))
	{
		move.x += 1;
	}
	if (pInput->IsActionTriggered(BACKWARD))
	{
		move.x -= 1;
	}

	if (move.x != 0 || move.y != 0)
	{
		DirectX::XMFLOAT4X4 view = gameContext.pCamera->GetView();
		DirectX::XMMATRIX viewMat = DirectX::XMLoadFloat4x4(&view);
		DirectX::XMVECTOR moveVec = DirectX::XMLoadFloat3(&move);

		DirectX::XMVector3Normalize(moveVec);
		DirectX::XMVector3TransformNormal(moveVec, viewMat);
		moveVec *= m_RunAcceleration * dt;

		DirectX::XMVECTOR veloVec = DirectX::XMLoadFloat3(&m_Velocity);
		veloVec += moveVec;
	}
	else
	{
		m_Velocity.x = 0;
		m_Velocity.z = 0;
	}
	



	


}