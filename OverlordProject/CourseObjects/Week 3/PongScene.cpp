#include "stdafx.h"
#include "PongScene.h"
#include "SpherePrefab.h"
#include "ColliderComponent.h"
#include "PhysxManager.h"
#include "FixedCamera.h"
#include "TransformComponent.h"
#include "RigidBodyComponent.h"
#include "CubePrefab.h"
#include "FreeCamera.h"
#include "../OverlordProject/CourseObjects/Week 3/Components/PaddleComponent.h"
#include "../OverlordProject/CourseObjects/Week 3/Components/BallComponent.h"
#include <time.h>

void WallPhysicsCallBack(GameObject* pTriggerObject, GameObject* pOtherGameObject, GameObject::TriggerAction triggerAction)
{
	if (triggerAction == GameObject::TriggerAction::ENTER)
	{
		BallComponent* pBallComponent{ pOtherGameObject->GetComponent<BallComponent>() };
		if (pBallComponent)
		{
			bool isBallZCoorBigger{pOtherGameObject->GetTransform()->GetPosition().z > pTriggerObject->GetTransform()->GetPosition().z};
			DirectX::XMFLOAT2 bounceNormal{ 0.f, isBallZCoorBigger ? -1.f : 1.f };
			pBallComponent->Bounce(bounceNormal);
			return;
		}
		PaddleComponent* pPaddleComponent{ pOtherGameObject->GetComponent<PaddleComponent>() };
		if (pPaddleComponent)
		{
			bool isPaddleZCoorBigger{ pOtherGameObject->GetTransform()->GetPosition().z > pTriggerObject->GetTransform()->GetPosition().z };
			if (isPaddleZCoorBigger)
			{
				pPaddleComponent->SetAgainstBottomWall();
			}
			else
			{
				pPaddleComponent->SetAgainstTopWall();
			}
		}
	}
}

void GoalPhysicsCallBack(GameObject*, GameObject* pOtherGameObject, GameObject::TriggerAction triggerAction)
{
	if (triggerAction == GameObject::TriggerAction::ENTER)
	{
		BallComponent* pBallComponent{ pOtherGameObject->GetComponent<BallComponent>() };
		if (pBallComponent)
		{
			pBallComponent->Reset();
		}
	}
}

void PaddlePhysicsCallBack(GameObject* pTriggerObject, GameObject* pOtherGameObject, GameObject::TriggerAction triggerAction)
{
	if (triggerAction == GameObject::TriggerAction::ENTER)
	{
		BallComponent* pBallComponent{ pOtherGameObject->GetComponent<BallComponent>() };
		PaddleComponent* pPaddleComponent{ pTriggerObject->GetComponent<PaddleComponent>() };
		if (pBallComponent && pPaddleComponent)
		{
			DirectX::XMFLOAT3 paddlePosition{ pTriggerObject->GetTransform()->GetPosition() };
			DirectX::XMFLOAT3 ballPosition{ pOtherGameObject->GetTransform()->GetPosition() };
			bool isBallXCoorBigger{ paddlePosition.x > ballPosition.x };
			float zCoorDifference{ paddlePosition.z - ballPosition.z };
			pBallComponent->Bounce(pPaddleComponent->GetNormal(zCoorDifference, isBallXCoorBigger));
			
		}
	}
}

PongScene::PongScene(const std::wstring& name)
	: GameScene{ name }
	, m_pBall{nullptr}
	, m_pLeftGoal{nullptr}
	, m_pRightGoal{nullptr}
{
}

void PongScene::Initialize()
{
	//std::srand(static_cast<int>(std::time(NULL)));
	const float ballRad{ 1.f };
	physx::PxPhysics* pPhysx{ PhysxManager::GetInstance()->GetPhysics() };
	physx::PxMaterial* pBaseMat{ pPhysx->createMaterial(1.f,1.f,1.f) };

	// MakeBall
	m_pBall = new SpherePrefab{ ballRad, 10, DirectX::XMFLOAT4{1.f,1.f,1.f,1.f} };
	std::shared_ptr<physx::PxGeometry> pBallGeom{ std::make_shared<physx::PxSphereGeometry>(ballRad) };
	RigidBodyComponent* pBallRB{ new RigidBodyComponent{} };
	pBallRB->SetKinematic(true);
	m_pBall->AddComponent(pBallRB);
	ColliderComponent* pBallCollider{ new ColliderComponent{ pBallGeom, *pBaseMat } };
	m_pBall->AddComponent(pBallCollider);
	BallComponent* pBallComponent{ new BallComponent{Start, 20, 0.5} };
	m_pBall->AddComponent(pBallComponent);
	m_pBall->GetTransform()->Translate(0, ballRad, 0);
	AddChild(m_pBall);

	// Make Ground Plane
	DirectX::XMFLOAT3 groundPlaneDim{1000, 1, 1000};
	CubePrefab* pGroundPlane{ new CubePrefab{groundPlaneDim.x, groundPlaneDim.y, groundPlaneDim.z, DirectX::XMFLOAT4{0,0,0,1}} };
	std::shared_ptr<physx::PxGeometry> pGroundPlaneGeom{ std::make_shared<physx::PxBoxGeometry>(groundPlaneDim.x / 2, groundPlaneDim.y / 2, groundPlaneDim.z / 2) };
	RigidBodyComponent* pGroundPlaneRB{ new RigidBodyComponent{true} };
	pGroundPlane->AddComponent(pGroundPlaneRB);
	ColliderComponent* pGroundPlaneCollider{ new ColliderComponent{pGroundPlaneGeom, *pBaseMat} };
	pGroundPlane->AddComponent(pGroundPlaneCollider);
	pGroundPlane->GetTransform()->Translate(0, -groundPlaneDim.y / 2, 0); // translate the cube so the top plane is equal to y: 0
	AddChild(pGroundPlane);

	// Make sides
	DirectX::XMFLOAT3 sideDim{100, 5, 1};
	std::shared_ptr<physx::PxGeometry> pSideGeom{ std::make_shared<physx::PxBoxGeometry>(sideDim.x / 2, sideDim.y / 2, sideDim.z / 2) };

	CubePrefab* pBottomSide{ new CubePrefab{sideDim.x, sideDim.y, sideDim.z, DirectX::XMFLOAT4{1,1,1,1}} };
	RigidBodyComponent* pBottomSideRB{ new RigidBodyComponent{true} };
	pBottomSide->AddComponent(pBottomSideRB);
	ColliderComponent* pBottomSideCollider{ new ColliderComponent{pSideGeom, *pBaseMat} };
	pBottomSide->AddComponent(pBottomSideCollider);
	pBottomSide->GetTransform()->Translate(0, 0, -20);
	pBottomSide->SetOnTriggerCallBack(GameObject::PhysicsCallback{ WallPhysicsCallBack });
	pBottomSideCollider->EnableTrigger(true);
	AddChild(pBottomSide);

	CubePrefab* pTopSide{ new CubePrefab{sideDim.x, sideDim.y, sideDim.z, DirectX::XMFLOAT4{1,1,1,1}} };
	RigidBodyComponent* pTopSideRB{ new RigidBodyComponent{true} };
	pTopSide->AddComponent(pTopSideRB);
	ColliderComponent* pTopSideCollider{ new ColliderComponent{pSideGeom, *pBaseMat} };
	pTopSideCollider->EnableTrigger(true);
	pTopSide->AddComponent(pTopSideCollider);
	pTopSide->GetTransform()->Translate(0, 0, 20);
	pTopSide->SetOnTriggerCallBack(GameObject::PhysicsCallback{ WallPhysicsCallBack });
	AddChild(pTopSide);

	// Make goals
	DirectX::XMFLOAT3 goalDim{5.f, 5.f, 50.f};
	std::shared_ptr<physx::PxGeometry> pGoalGeom{ std::make_shared<physx::PxBoxGeometry>(goalDim.x / 2, goalDim.y / 2, goalDim.z / 2) };

	m_pLeftGoal = new GameObject{};
	m_pLeftGoal->GetTransform()->Translate(-45, 0, 0); // Translate before adding static rigidbody
	RigidBodyComponent* pLeftGoalRB{ new RigidBodyComponent{true} };
	m_pLeftGoal->AddComponent(pLeftGoalRB);
	ColliderComponent* pLeftGoalCollider{ new ColliderComponent{pGoalGeom, *pBaseMat} };
	pLeftGoalCollider->EnableTrigger(true);
	m_pLeftGoal->AddComponent(pLeftGoalCollider);
	m_pLeftGoal->SetOnTriggerCallBack(GoalPhysicsCallBack);
	AddChild(m_pLeftGoal);

	m_pRightGoal = new GameObject{};
	m_pRightGoal->GetTransform()->Translate(45, 0, 0);
	RigidBodyComponent* pRightGoalRB{ new RigidBodyComponent{true} };
	m_pRightGoal->AddComponent(pRightGoalRB);
	ColliderComponent* pRightGoalCollider{ new ColliderComponent{ pGoalGeom, *pBaseMat } };
	pRightGoalCollider->EnableTrigger(true);
	m_pRightGoal->AddComponent(pRightGoalCollider);
	m_pRightGoal->SetOnTriggerCallBack(GoalPhysicsCallBack);
	AddChild(m_pRightGoal);

	// Make paddles
	const float paddleSpeed{10.f};
	const float maxNormalY{ 0.5f };
	DirectX::XMFLOAT3 paddleDim{ 2.f, 5.f, 5.f };
	std::shared_ptr<physx::PxGeometry> pPaddleGeom{ std::make_shared<physx::PxBoxGeometry>(paddleDim.x / 2, paddleDim.y / 2, paddleDim.z / 2) };

	CubePrefab* pLeftPaddle = new CubePrefab{ paddleDim.x, paddleDim.y, paddleDim.z, DirectX::XMFLOAT4{1,1,1,1} };
	pLeftPaddle->GetTransform()->Translate(-40, 0, 0);
	RigidBodyComponent* pLeftPaddleRB{ new RigidBodyComponent{} };
	pLeftPaddleRB->SetKinematic(true);
	pLeftPaddle->AddComponent(pLeftPaddleRB);
	ColliderComponent* pLeftPaddleCollider{ new ColliderComponent{pPaddleGeom, *pBaseMat} };
	pLeftPaddleCollider->EnableTrigger(true);
	pLeftPaddle->AddComponent(pLeftPaddleCollider);
	PaddleComponent* pLeftPaddleComponent{ new PaddleComponent{m_InputIds::LeftUp, m_InputIds::LeftDown, paddleDim.z, paddleSpeed, maxNormalY} };
	pLeftPaddle->AddComponent(pLeftPaddleComponent);
	pLeftPaddle->SetOnTriggerCallBack(PaddlePhysicsCallBack);
	AddChild(pLeftPaddle);

	CubePrefab* pRightPaddle = new CubePrefab{ paddleDim.x, paddleDim.y, paddleDim.z, DirectX::XMFLOAT4{1,1,1,1} };
	pRightPaddle->GetTransform()->Translate(40, 0, 0);
	RigidBodyComponent* pRightPaddleRB{ new RigidBodyComponent{} };
	pRightPaddleRB->SetKinematic(true);
	pRightPaddle->AddComponent(pRightPaddleRB);
	ColliderComponent* pRightPaddleCollider{ new ColliderComponent{pPaddleGeom, *pBaseMat} };
	pRightPaddleCollider->EnableTrigger(true);
	pRightPaddle->AddComponent(pRightPaddleCollider);
	PaddleComponent* pRightPaddleComponent{ new PaddleComponent{m_InputIds::RightUp, m_InputIds::RightDown, paddleDim.z, paddleSpeed, maxNormalY} };
	pRightPaddle->AddComponent(pRightPaddleComponent);
	pRightPaddle->SetOnTriggerCallBack(PaddlePhysicsCallBack);
	AddChild(pRightPaddle);

	// Set Camera
	GameObject* pCamera{ new FixedCamera{} };
	AddChild(pCamera);
	pCamera->GetTransform()->Translate(0,60,0);
	pCamera->GetTransform()->Rotate(DirectX::XMFLOAT3{ 90,0,0 });
	SetActiveCamera(pCamera->GetComponent<CameraComponent>());

	// MakeInputs
	InputAction leftUpAction{ m_InputIds::LeftUp, InputTriggerState::Down, 87 };
	GetGameContext().pInput->AddInputAction(leftUpAction);

	InputAction leftDownAction{ m_InputIds::LeftDown, InputTriggerState::Down, 83 };
	GetGameContext().pInput->AddInputAction(leftDownAction);

	InputAction rightUpAction{ m_InputIds::RightUp, InputTriggerState::Down, 38 };
	GetGameContext().pInput->AddInputAction(rightUpAction);

	InputAction rightDownAction{ m_InputIds::RightDown, InputTriggerState::Down, 40 };
	GetGameContext().pInput->AddInputAction(rightDownAction);

	InputAction startAction{ m_InputIds::Start, InputTriggerState::Pressed, 32 };
	GetGameContext().pInput->AddInputAction(startAction);
}

void PongScene::Update()
{
}

void PongScene::Draw()
{
}

