#pragma once
#include "BaseComponent.h"

class PaddleComponent final : public BaseComponent
{
public:
	PaddleComponent(int upActionId, int downActionId, float width, float speed, float maxNormalY);
	DirectX::XMFLOAT2 GetNormal(float zCoorDifference, bool isXCoorBigger) const;
	void SetAgainstTopWall();
	void SetAgainstBottomWall();

private:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;

	int m_UpActionId;
	int m_DownActionId;
	float m_Width;
	float m_Speed;
	float m_MaxNormalY;
	bool m_AgainstTopWall;
	bool m_AgainstBottomWall;
};