#include "stdafx.h"
#include "BallComponent.h"
#include "GameObject.h"
#include "TransformComponent.h"
#include "MathHelper.h"

BallComponent::BallComponent(int startActionId, float speed, float maxZMovement)
	: m_StartActionId{startActionId}
	, m_Speed{speed}
	, m_MaxZMovement{maxZMovement}
	, m_Velocity{ 0,0 }
	, m_Active{false}
	, m_LeftPlayerStart{true}
{
}

void BallComponent::Bounce(const DirectX::XMFLOAT2& normal)
{
	DirectX::XMVECTOR normalVec = DirectX::XMLoadFloat2(&normal);
	DirectX::XMVECTOR velVec = DirectX::XMLoadFloat2(&m_Velocity);
	DirectX::XMVector2Normalize(normalVec);
	velVec = DirectX::XMVector2Reflect(velVec, normalVec);
	DirectX::XMStoreFloat2(&m_Velocity, velVec);
}

void BallComponent::Reset()
{
	m_pGameObject->GetTransform()->Translate(0, 0, 0);
	m_Active = false;
	m_Velocity = DirectX::XMFLOAT2{ 0.f,0.f };
}

void BallComponent::Initialize(const GameContext&)
{
}

void BallComponent::Update(const GameContext& gameContext)
{
	if (!m_Active && gameContext.pInput->IsActionTriggered(m_StartActionId))
	{
		LaunchBall();
	}

	if (m_Active)
	{
		const float elapsedSec = gameContext.pGameTime->GetElapsed();
		const auto& pos = m_pGameObject->GetTransform()->GetPosition();

		m_pGameObject->GetTransform()->Translate(pos.x + m_Velocity.x * elapsedSec, pos.y, pos.z + m_Velocity.y * elapsedSec);
	}
}

void BallComponent::Draw(const GameContext&)
{
}

void BallComponent::LaunchBall()
{
	using namespace DirectX; // we need to use the namespace to not have problems with operator overloading

	m_Active = true;
	float zVal = randF(-m_MaxZMovement, m_MaxZMovement);
	m_Velocity = { 1, zVal };
	DirectX::XMVECTOR velVector = DirectX::XMLoadFloat2(&m_Velocity);
	velVector = DirectX::XMVector2Normalize(velVector);
	velVector = (m_LeftPlayerStart ? -m_Speed : m_Speed) * velVector; // tertiary operator decides what direction the ball should be launched

	DirectX::XMStoreFloat2(&m_Velocity, velVector);

	m_LeftPlayerStart = !m_LeftPlayerStart; // next launch fires toward the other player
}
