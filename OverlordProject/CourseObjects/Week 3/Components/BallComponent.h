#pragma once
#include "BaseComponent.h"
#include <DirectXMath.h>

class BallComponent final : public BaseComponent
{
public:
	BallComponent(int startActionId, float speed, float maxZMovement);
	void Bounce(const DirectX::XMFLOAT2& normal);
	void Reset();

private:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;

	void LaunchBall();


	int m_StartActionId;
	float m_Speed;
	float m_MaxZMovement;
	DirectX::XMFLOAT2 m_Velocity;
	bool m_Active;
	bool m_LeftPlayerStart;
};