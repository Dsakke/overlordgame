#include "stdafx.h"
#include "PaddleComponent.h"
#include "GameObject.h"
#include "GameScene.h"
#include "TransformComponent.h"

PaddleComponent::PaddleComponent(int upActionId, int downActionId, float width, float speed, float maxNormalY)
	: m_UpActionId{upActionId}
	, m_DownActionId{downActionId}
	, m_Width{width}
	, m_Speed{speed}
	, m_MaxNormalY{maxNormalY}
	, m_AgainstTopWall{false}
	, m_AgainstBottomWall{false}
{
}

DirectX::XMFLOAT2 PaddleComponent::GetNormal(float zCoorDifference, bool isXCoorBigger) const
{
	DirectX::XMFLOAT2 normal{isXCoorBigger ? 1.f : -1.f, 0};
	normal.y = (zCoorDifference / (m_Width / 2)) * m_MaxNormalY;
	DirectX::XMVECTOR normalVec = DirectX::XMLoadFloat2(&normal);
	DirectX::XMVector2Normalize(normalVec);
	DirectX::XMStoreFloat2(&normal, normalVec);
	return normal;
}

void PaddleComponent::SetAgainstTopWall()
{
	m_AgainstTopWall = true;
}

void PaddleComponent::SetAgainstBottomWall()
{
	m_AgainstBottomWall = true;
}

void PaddleComponent::Initialize(const GameContext&)
{
}

void PaddleComponent::Update(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(m_UpActionId))
	{
		if (!m_AgainstTopWall)
		{
			DirectX::XMFLOAT3 pos{ m_pGameObject->GetTransform()->GetPosition() };
			m_pGameObject->GetTransform()->Translate(pos.x, pos.y, pos.z + m_Speed * gameContext.pGameTime->GetElapsed());
			m_AgainstBottomWall = false;
		}
	}

	if (gameContext.pInput->IsActionTriggered(m_DownActionId))
	{
		if (!m_AgainstBottomWall)
		{
			DirectX::XMFLOAT3 pos{ m_pGameObject->GetTransform()->GetPosition() };
			m_pGameObject->GetTransform()->Translate(pos.x, pos.y, pos.z - m_Speed * gameContext.pGameTime->GetElapsed());
			m_AgainstTopWall = false;
		}
	}

}

void PaddleComponent::Draw(const GameContext&)
{
}

