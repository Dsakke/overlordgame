#pragma once
#include "GameScene.h"
#include <string>


class SpherePrefab;
class PongScene final : public GameScene
{
public:
	PongScene(const std::wstring& name);
	~PongScene() = default;


private:
	void Initialize() override;
	void Update() override;
	void Draw() override;

	SpherePrefab* m_pBall;

	GameObject* m_pLeftGoal;
	GameObject* m_pRightGoal;

	enum m_InputIds : int
	{
		LeftUp,
		LeftDown,
		RightUp,
		RightDown,
		Start
	};
};