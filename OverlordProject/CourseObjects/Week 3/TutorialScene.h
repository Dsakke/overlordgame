#pragma once
#include "GameScene.h"
class TorusPrefab;
class TutorialScene final : public GameScene
{
public:
	TutorialScene();
	virtual ~TutorialScene() = default;

	TutorialScene(const TutorialScene& other) = delete;
	TutorialScene(TutorialScene&& other) noexcept = delete;
	TutorialScene& operator=(const TutorialScene& other) = delete;
	TutorialScene& operator=(TutorialScene&& other) noexcept = delete;

protected:
	void Initialize() override;
	void Update() override;
	void Draw() override;

	TorusPrefab* m_pTorus1;
	TorusPrefab* m_pTorus2;
	TorusPrefab* m_pTorus3;
};


