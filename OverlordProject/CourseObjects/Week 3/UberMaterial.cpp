#include "stdafx.h"
#include "UberMaterial.h"
#include "ContentManager.h"
#include "TextureData.h"

ID3DX11EffectVectorVariable* UberMaterial::m_pLightDirectionVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUseDiffuseTextureVariable{};
ID3DX11EffectShaderResourceVariable* UberMaterial::m_pDiffuseSRVvariable{};
ID3DX11EffectVectorVariable* UberMaterial::m_pDiffuseColorVariable{};
ID3DX11EffectVectorVariable* UberMaterial::m_pSpecularColorVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUseSpecularLevelTextureVariable{};
ID3DX11EffectShaderResourceVariable* UberMaterial::m_pSpecularLevelSRVvariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pShininessVariable{};
ID3DX11EffectVectorVariable* UberMaterial::m_pAmbientColorVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pAmbientIntensityVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pFlipGreenChannelVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUseNormalMappingVariable{};
ID3DX11EffectShaderResourceVariable* UberMaterial::m_pNormalMappingSRVvariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUseEnvironmentMappingVariable{};
ID3DX11EffectShaderResourceVariable* UberMaterial::m_pEnvironmentSRVvariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pReflectionStrengthVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pRefractionStrengthVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pRefractionIndexVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pOpacityVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUseOpacityMapVariable{};
ID3DX11EffectShaderResourceVariable* UberMaterial::m_pOpacitySRVvariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUseBlinnVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUsePhongVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pUseFresnelFalloffVariable{};
ID3DX11EffectVectorVariable* UberMaterial::m_pFresnelColorVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pFresnelPowerVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pFresnelMultiplierVariable{};
ID3DX11EffectScalarVariable* UberMaterial::m_pFresnelHardnessVariable{};

UberMaterial::UberMaterial()
	: Material{ L"Resources/effects/UberMaterial.fx", L"WithAlphaBlending" }
	, m_LightDirection{}
	, m_bDiffuseTexture{false}
	, m_pDiffuseTexture{false}
	, m_ColorDiffuse{}
	, m_ColorSpecular{}
	, m_bSpecularLevelTexture{false}
	, m_pSpecularLevelTexture{nullptr}
	, m_Shininess{15}
	, m_ColorAmbient{}
	, m_AmbientIntensity{0.2f}
	, m_bFlipGreenChannel{false}
	, m_bNormalMapping{false}
	, m_pNormalMappingTexture{nullptr}
	, m_bEnvironmentMapping{false}
	, m_pEnvironmentCube{nullptr}
	, m_ReflectionStrength{0.2f}
	, m_RefractionStrength{0.2f}
	, m_RefractionIndex{0.2f}
	, m_Opacity{1.f}
	, m_bOpacityMap{false}
	, m_pOpacityMap{nullptr}
	, m_bSpecularBlinn{true}
	, m_bSpecularPhong{false}
	, m_bFresnelFalloff{true}
	, m_ColorFresnel{1,1,1,1}
	, m_FresnelPower{1.f}
	, m_FresnelMultiplier{1.f}
	, m_FresnelHardness{1.f}
{
}

UberMaterial::~UberMaterial()
{
}

void UberMaterial::SetLightDirection(DirectX::XMFLOAT3 direction)
{
	m_LightDirection = direction;
}

void UberMaterial::EnableDiffuseTexture(bool enable)
{
	m_bDiffuseTexture = enable;
}

void UberMaterial::SetDiffuseTexture(const std::wstring& assetFile)
{
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void UberMaterial::SetDiffuseColor(DirectX::XMFLOAT4 color)
{
	m_ColorDiffuse = color;
}

void UberMaterial::SetSpecularColor(DirectX::XMFLOAT4 color)
{
	m_ColorSpecular = color;
}

void UberMaterial::EnableSpecularLevelTexture(bool enable)
{
	m_bSpecularLevelTexture = enable;
}

void UberMaterial::SetSpecularLevelTexture(const std::wstring& assetFile)
{
	m_pSpecularLevelTexture = ContentManager::Load<TextureData>(assetFile);
}

void UberMaterial::SetShininess(int shininess)
{
	m_Shininess = shininess;
}

void UberMaterial::SetAmbientColor(DirectX::XMFLOAT4 color)
{
	m_ColorAmbient = color;
}

void UberMaterial::SetAmbientIntensity(float intensity)
{
	m_AmbientIntensity = intensity;
}

void UberMaterial::FlipNormalGreenCHannel(bool flip)
{
	m_bFlipGreenChannel = flip;
}

void UberMaterial::EnableNormalMapping(bool enable)
{
	m_bNormalMapping = enable;
}

void UberMaterial::SetNormalMapTexture(const std::wstring& assetFile)
{
	m_pNormalMappingTexture = ContentManager::Load<TextureData>(assetFile);
}

void UberMaterial::EnableEnvironmentMapping(bool enable)
{
	m_bEnvironmentMapping = enable;
}

void UberMaterial::SetEnvironmentCube(const std::wstring& assetFile)
{
	m_pEnvironmentCube = ContentManager::Load<TextureData>(assetFile);
}

void UberMaterial::SetReflectionStrength(float strength)
{
	m_ReflectionStrength = strength;
}

void UberMaterial::SetRefractionStrength(float strength)
{
	m_RefractionStrength = strength;
}

void UberMaterial::SetRefractionIndex(float index)
{
	m_RefractionIndex = index;
}

void UberMaterial::SetOpacity(float opacity)
{
	m_Opacity = opacity;
}

void UberMaterial::EnableOpacityMap(bool enable)
{
	m_bOpacityMap = enable;
}

void UberMaterial::SetOpacityTexture(const std::wstring& assetFile)
{
	m_pOpacityMap = ContentManager::Load<TextureData>(assetFile);
}

void UberMaterial::EnableSpecularBlinn(bool enable)
{
	m_bSpecularBlinn = enable;
}

void UberMaterial::EnableSpecularPhong(bool enable)
{
	m_bSpecularPhong = enable;
}

void UberMaterial::EnableFresnelFaloff(bool enable)
{
	m_bFresnelFalloff = enable;
}

void UberMaterial::SetFresnelColor(DirectX::XMFLOAT4 color)
{
	m_ColorFresnel = color;
}

void UberMaterial::SetFresnelPower(float power)
{
	m_FresnelPower = power;
}

void UberMaterial::SetFresnelMultiplier(float multiplier)
{
	m_FresnelMultiplier = multiplier;
}

void UberMaterial::SetFresnelHardness(float hardness)
{
	m_FresnelHardness = hardness;
}

void UberMaterial::LoadEffectVariables()
{
	// Light
	m_pLightDirectionVariable = GetEffect()->GetVariableByName("gLightDirection")->AsVector();

	// Diffuse
	m_pUseDiffuseTextureVariable = GetEffect()->GetVariableByName("gUseTextureDiffuse")->AsScalar();
	m_pDiffuseSRVvariable = GetEffect()->GetVariableByName("gTextureDiffuse")->AsShaderResource();
	m_pDiffuseColorVariable = GetEffect()->GetVariableByName("gColorDiffuse")->AsVector();

	// Specular
	m_pSpecularColorVariable = GetEffect()->GetVariableByName("gColorSpecular")->AsVector();
	m_pUseSpecularLevelTextureVariable = GetEffect()->GetVariableByName("gUseTextureSpecularIntensity")->AsScalar();
	m_pSpecularLevelSRVvariable = GetEffect()->GetVariableByName("gTextureSpecularIntensity")->AsShaderResource();
	m_pShininessVariable = GetEffect()->GetVariableByName("gShininess")->AsScalar();

	// Ambient
	m_pAmbientColorVariable = GetEffect()->GetVariableByName("gColorAmbient")->AsVector();
	m_pAmbientIntensityVariable = GetEffect()->GetVariableByName("gAmbientIntensity")->AsScalar();

	// Normal mapping
	m_pFlipGreenChannelVariable = GetEffect()->GetVariableByName("gFlipGreenChannel")->AsScalar();
	m_pUseNormalMappingVariable = GetEffect()->GetVariableByName("gUseTextureNormal")->AsScalar();
	m_pNormalMappingSRVvariable = GetEffect()->GetVariableByName("gTextureNormal")->AsShaderResource();

	// Enviroment mapping
	m_pUseEnvironmentMappingVariable = GetEffect()->GetVariableByName("gUseEnvironmentMapping")->AsScalar();
	m_pEnvironmentSRVvariable = GetEffect()->GetVariableByName("gCubeEnvironment")->AsShaderResource();
	m_pReflectionStrengthVariable = GetEffect()->GetVariableByName("gReflectionStrength")->AsScalar();
	m_pRefractionStrengthVariable = GetEffect()->GetVariableByName("gRefractionStrength")->AsScalar();
	m_pRefractionIndexVariable = GetEffect()->GetVariableByName("gRefractionIndex")->AsScalar();

	// Opacity
	m_pOpacityVariable = GetEffect()->GetVariableByName("gOpacityIntensity")->AsScalar();
	m_pUseOpacityMapVariable = GetEffect()->GetVariableByName("gTextureOpacityIntensity")->AsScalar();
	m_pOpacitySRVvariable = GetEffect()->GetVariableByName("gTextureOpacity")->AsShaderResource();

	// Specular Models
	m_pUseBlinnVariable = GetEffect()->GetVariableByName("gUseSpecularBlinn")->AsScalar();
	m_pUsePhongVariable = GetEffect()->GetVariableByName("gUseSpecularPhong")->AsScalar();

	// Fresnel falloff
	m_pUseFresnelFalloffVariable = GetEffect()->GetVariableByName("gUseFresnelFalloff")->AsScalar();
	m_pFresnelColorVariable = GetEffect()->GetVariableByName("gColorFresnel")->AsVector();
	m_pFresnelPowerVariable = GetEffect()->GetVariableByName("gFresnelPower")->AsScalar();
	m_pFresnelMultiplierVariable = GetEffect()->GetVariableByName("gFresnelMultiplier")->AsScalar();
	m_pFresnelHardnessVariable = GetEffect()->GetVariableByName("gFresnelHardness")->AsScalar();
}

void UberMaterial::UpdateEffectVariables(const GameContext& , ModelComponent* )
{
	// Light
	m_pLightDirectionVariable->SetFloatVector((float*)&m_LightDirection);

	// Diffuse
	m_pUseDiffuseTextureVariable->SetBool(m_bDiffuseTexture);
	if(m_bDiffuseTexture)
		m_pDiffuseSRVvariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
	m_pDiffuseColorVariable->SetFloatVector((float*)&m_ColorDiffuse);

	// Specular
	m_pSpecularColorVariable->SetFloatVector((float*)&m_ColorSpecular);
	m_pUseSpecularLevelTextureVariable->SetBool(m_bSpecularLevelTexture);
	if(m_bSpecularLevelTexture)
		m_pSpecularLevelSRVvariable->SetResource(m_pSpecularLevelTexture->GetShaderResourceView());
	m_pShininessVariable->SetInt(m_Shininess);

	// Ambient
	m_pAmbientColorVariable->SetFloatVector((float*)&m_ColorAmbient);
	m_pAmbientIntensityVariable->SetFloat(m_AmbientIntensity);

	// Normal Mapping
	m_pFlipGreenChannelVariable->SetBool(m_bFlipGreenChannel);
	m_pUseNormalMappingVariable->SetBool(m_bNormalMapping);
	if(m_bNormalMapping)
		m_pNormalMappingSRVvariable->SetResource(m_pNormalMappingTexture->GetShaderResourceView());

	// Enviroment mapping
	m_pUseEnvironmentMappingVariable->SetBool(m_bEnvironmentMapping);
	if(m_bEnvironmentMapping)
		m_pEnvironmentSRVvariable->SetResource(m_pEnvironmentCube->GetShaderResourceView());
	m_pReflectionStrengthVariable->SetFloat(m_ReflectionStrength);
	m_pRefractionStrengthVariable->SetFloat(m_RefractionStrength);
	m_pRefractionIndexVariable->SetFloat(m_RefractionIndex);

	// Opacity
	m_pOpacityVariable->SetFloat(m_Opacity);
	m_pUseOpacityMapVariable->SetBool(m_bOpacityMap);
	if(m_bOpacityMap)
		m_pOpacitySRVvariable->SetResource(m_pOpacityMap->GetShaderResourceView());

	// Specular
	m_pUseBlinnVariable->SetBool(m_bSpecularBlinn);
	m_pUsePhongVariable->SetBool(m_bSpecularPhong);

	// Fresnel falloff
	m_pUseFresnelFalloffVariable->SetBool(m_bFresnelFalloff);
	m_pFresnelColorVariable->SetFloatVector((float*)&m_ColorFresnel);
	m_pFresnelPowerVariable->SetFloat(m_FresnelPower);
	m_pFresnelMultiplierVariable->SetFloat(m_FresnelMultiplier);
	m_pFresnelHardnessVariable->SetFloat(m_FresnelHardness);
}
