#include "stdafx.h"
#include "TutorialScene.h"
#include "TorusPrefab.h"
#include "TransformComponent.h"


TutorialScene::TutorialScene()
	: GameScene{L"TutorialScene"}
{
}

void TutorialScene::Initialize()
{
	Logger::LogInfo(L"Welcome, humble Minion!");
	m_pTorus1 = new TorusPrefab(5.f);
	AddChild(m_pTorus1);
	m_pTorus2 = new TorusPrefab(3.f);
	AddChild(m_pTorus2);
	m_pTorus3 = new TorusPrefab(1.f);
	AddChild(m_pTorus3);
}

void TutorialScene::Update()
{
}

void TutorialScene::Draw()
{
}
