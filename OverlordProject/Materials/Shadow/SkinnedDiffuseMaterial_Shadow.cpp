//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "SkinnedDiffuseMaterial_Shadow.h"
#include "GeneralStructs.h"
#include "Logger.h"
#include "ContentManager.h"
#include "TextureData.h"
#include "ModelComponent.h"
#include "ModelAnimator.h"
#include "Components.h"

ID3DX11EffectShaderResourceVariable* SkinnedDiffuseMaterial_Shadow::m_pDiffuseSRVvariable = nullptr;
ID3DX11EffectMatrixVariable* SkinnedDiffuseMaterial_Shadow::m_pBoneTransforms = nullptr;
ID3DX11EffectVectorVariable* SkinnedDiffuseMaterial_Shadow::m_pLightDirectionVariable = nullptr;
ID3DX11EffectShaderResourceVariable* SkinnedDiffuseMaterial_Shadow::m_pShadowSRVvariable = nullptr;
ID3DX11EffectMatrixVariable* SkinnedDiffuseMaterial_Shadow::m_pLightWVPvariable = nullptr;

SkinnedDiffuseMaterial_Shadow::SkinnedDiffuseMaterial_Shadow() : Material(L"./Resources/Effects/Shadow/PosNormTex3D_Skinned_Shadow.fx"),
	m_pDiffuseTexture(nullptr)
{}

void SkinnedDiffuseMaterial_Shadow::SetDiffuseTexture(const std::wstring& assetFile)
{
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void SkinnedDiffuseMaterial_Shadow::SetLightDirection(DirectX::XMFLOAT3 dir)
{
	m_LightDirection = dir;
}

void SkinnedDiffuseMaterial_Shadow::LoadEffectVariables()
{
	ID3DX11Effect* pEffect = GetEffect();

	m_pDiffuseSRVvariable = pEffect->GetVariableByName("gDiffuseMap")->AsShaderResource();
	if (!m_pDiffuseSRVvariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gDiffuseMap not found in effect");
	}

	m_pShadowSRVvariable = pEffect->GetVariableByName("gShadowMap")->AsShaderResource();
	if (!m_pShadowSRVvariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gShadowMap not found in effect");
	}

	m_pLightDirectionVariable = pEffect->GetVariableByName("gLightDirection")->AsVector();
	if (!m_pLightDirectionVariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gLightDirection not found in effect");
	}

	m_pLightWVPvariable = pEffect->GetVariableByName("gWorldViewProj_Light")->AsMatrix();
	if (!m_pLightWVPvariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gWorldViewProj_Light not found in effect");
	}

	m_pBoneTransforms = pEffect->GetVariableByName("gBones")->AsMatrix();
	if (!m_pBoneTransforms->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gBones not found in effect");
	}
}

void SkinnedDiffuseMaterial_Shadow::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent)
{
	m_pDiffuseSRVvariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
	m_pShadowSRVvariable->SetResource(gameContext.pShadowMapper->GetShadowMap());
	m_pLightDirectionVariable->SetFloatVector(reinterpret_cast<float*>(&m_LightDirection));
	DirectX::XMFLOAT4X4 lightVP = gameContext.pShadowMapper->GetLightVP();
	const DirectX::XMFLOAT4X4& world = pModelComponent->GetTransform()->GetWorld();
	DirectX::XMMATRIX lightVPMat = DirectX::XMLoadFloat4x4(&lightVP);
	DirectX::XMMATRIX worldMat = DirectX::XMLoadFloat4x4(&world);
	DirectX::XMMATRIX lightWVPMat = worldMat * lightVPMat;
	DirectX::XMFLOAT4X4 lightWVP{};
	DirectX::XMStoreFloat4x4(&lightWVP, lightWVPMat);
	m_pLightWVPvariable->SetMatrix(reinterpret_cast<float*>(&lightWVP));

	// Bone update
	auto boneTransforms = pModelComponent->GetAnimator()->GetBoneTransforms();
	m_pBoneTransforms->SetMatrixArray(reinterpret_cast<float*>(boneTransforms.data()), 0, boneTransforms.size());
}