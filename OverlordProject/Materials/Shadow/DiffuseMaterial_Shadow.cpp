//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "DiffuseMaterial_Shadow.h"
#include "GeneralStructs.h"
#include "Logger.h"
#include "ContentManager.h"
#include "TextureData.h"
#include "Components.h"

ID3DX11EffectShaderResourceVariable* DiffuseMaterial_Shadow::m_pDiffuseSRVvariable = nullptr;
ID3DX11EffectShaderResourceVariable* DiffuseMaterial_Shadow::m_pShadowSRVvariable = nullptr;
ID3DX11EffectVectorVariable* DiffuseMaterial_Shadow::m_pLightDirectionVariable = nullptr;
ID3DX11EffectMatrixVariable* DiffuseMaterial_Shadow::m_pLightWVPvariable = nullptr;

DiffuseMaterial_Shadow::DiffuseMaterial_Shadow() : Material(L"./Resources/Effects/Shadow/PosNormTex3D_Shadow.fx"),
	m_pDiffuseTexture(nullptr)
{}

void DiffuseMaterial_Shadow::SetDiffuseTexture(const std::wstring& assetFile)
{
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void DiffuseMaterial_Shadow::SetLightDirection(DirectX::XMFLOAT3 dir)
{
	m_LightDirection = dir;
}

void DiffuseMaterial_Shadow::LoadEffectVariables()
{
	ID3DX11Effect* pEffect = GetEffect();

	m_pDiffuseSRVvariable = pEffect->GetVariableByName("gDiffuseMap")->AsShaderResource();
	if (!m_pDiffuseSRVvariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gDiffuseMap not found in effect");
	}
	else
	{
		m_pDiffuseSRVvariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
	}

	m_pShadowSRVvariable = pEffect->GetVariableByName("gShadowMap")->AsShaderResource();
	if (!m_pShadowSRVvariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gShadowMap not found in effect");
	}

	m_pLightDirectionVariable = pEffect->GetVariableByName("gLightDirection")->AsVector();
	if (!m_pLightDirectionVariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gLightDirection not found in effect");
	}
	else
	{
		m_pLightDirectionVariable->SetFloatVector((float*)&m_LightDirection);
	}


	m_pLightWVPvariable = pEffect->GetVariableByName("gWorldViewProj_Light")->AsMatrix();
	if (!m_pLightWVPvariable->IsValid())
	{
		Logger::LogWarning(L"DiffuseMaterual_Shadow::LoadEffectVariables() >> Variable gWorldViewProj_Light not found in effect");
	}
}

void DiffuseMaterial_Shadow::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent)
{
	m_pLightDirectionVariable->SetFloatVector(reinterpret_cast<float*>(&m_LightDirection));
	m_pShadowSRVvariable->SetResource(gameContext.pShadowMapper->GetShadowMap());
	m_pDiffuseSRVvariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());

	DirectX::XMFLOAT4X4 world{ pModelComponent->GetTransform()->GetWorld() };
	DirectX::XMMATRIX worldMat{ DirectX::XMLoadFloat4x4(&pModelComponent->GetTransform()->GetWorld()) };
	DirectX::XMFLOAT4X4 lightVP{ gameContext.pShadowMapper->GetLightVP() };
	DirectX::XMMATRIX lightVPMat{ DirectX::XMLoadFloat4x4(&lightVP) };
	DirectX::XMFLOAT4X4 lightWVP{   };
	DirectX::XMStoreFloat4x4(&lightWVP, worldMat * lightVPMat);
	m_pLightWVPvariable->SetMatrix((reinterpret_cast<float*>(&lightWVP)));
}