float4x4 gWorld : WORLD;
float4x4 gView : View;
float4x4 gViewInverse : ViewInverse;
float4x4 gWorldViewProj : WORLDVIEWPROJECTION; 
float4x4 gWorldViewProj_Light;
float3 gLightDirection = float3(-0.577f, -0.577f, 0.577f);
float gShadowMapBias = 0.01f;
float4x4 gBones[70];
float gLightBias = 0.01f;

Texture2D gDiffuseMap;
Texture2D gShadowMap;

SamplerComparisonState cmpSampler
{
	// sampler state
	Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;

	// sampler comparison state
	ComparisonFunc = LESS_EQUAL;
};

SamplerState samLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;// or Mirror or Clamp or Border
    AddressV = Wrap;// or Mirror or Clamp or Border
};

struct VS_INPUT
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
	float2 texCoord : TEXCOORD;
	float4 BoneIndices : BLENDINDICES;
	float4 BoneWeights : BLENDWEIGHTS;
};

struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
	float3 normal : NORMAL;
	float2 texCoord : TEXCOORD;
	float4 lPos : TEXCOORD1;
};

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

RasterizerState NoCulling
{
	CullMode = NONE;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
    float4 transformedPos = 0;
    float3 transformedNorm = 0;
	//TODO: complete Vertex Shader 
	//Hint: use the previously made shaders PosNormTex3D_Shadow and PosNormTex3D_Skinned as a guide
    for (int i = 0; i < 4; ++i)
    {
        if (input.BoneIndices[i] > -1)
        {
            transformedPos += mul(float4(input.pos, 1), gBones[(int) input.BoneIndices[i]]) * input.BoneWeights[i];
            transformedNorm += mul(input.normal, (float3x3) gBones[(int) input.BoneIndices[i]]) * input.BoneWeights[i];
        }
    }
    output.pos = mul(transformedPos, gWorldViewProj);
    output.lPos = mul(transformedPos, gWorldViewProj_Light);
    output.normal = normalize(mul(transformedNorm, (float3x3) gWorld));
    output.texCoord = input.texCoord;
	return output;
}

float2 texOffset(int u, int v)
{
	//TODO: return offseted value (our shadow map has the following dimensions: 1280 * 720)
    return float2(u, v);
}

float EvaluateShadowMap(float4 lpos)
{
	//TODO: complete
    lpos.xyz = lpos.xyz / lpos.w;
    bool isOutsideFrustrumX = lpos.x < -1.f || lpos.x > 1.f;
    bool isOutsideFrustrumY = lpos.y < -1.f || lpos.y > 1.f;
    bool isOutsideFrustrumZ = lpos.z < -1.f || lpos.z > 1.f;
    if (isOutsideFrustrumX || isOutsideFrustrumY || isOutsideFrustrumZ)
    {
        return 0.0f;
    }
    float2 texCoords = (float2)0;
    texCoords.x = lpos.x / 2 + 0.5f;
    texCoords.y = lpos.y / -2 + 0.5f;
    lpos.z -= gLightBias;
    float shadowDepth = gShadowMap.Sample(samLinear, texCoords).r;
    if (shadowDepth < lpos.z)
    {
        return 0.f;
    }
    return 1.0f;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_TARGET
{
	float shadowValue = EvaluateShadowMap(input.lPos);

	float4 diffuseColor = gDiffuseMap.Sample( samLinear,input.texCoord );
	float3 color_rgb = diffuseColor.rgb;
	float color_a = diffuseColor.a;
	
	//HalfLambert Diffuse :)
	float diffuseStrength = dot(input.normal, -gLightDirection);
	diffuseStrength = diffuseStrength * 0.5 + 0.5;
	diffuseStrength = saturate(diffuseStrength);
	color_rgb = color_rgb * diffuseStrength;

	return float4( color_rgb * shadowValue, color_a );
}

//--------------------------------------------------------------------------------------
// Technique
//--------------------------------------------------------------------------------------
technique11 Default
{
    pass P0
    {
		SetRasterizerState(NoCulling);
		SetDepthStencilState(EnableDepth, 0);

		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}

