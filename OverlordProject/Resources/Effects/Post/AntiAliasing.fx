//=============================================================================
//// Shader uses position and texture
//=============================================================================


// Edge detection is based on this: https://github.com/iryoku/smaa/blob/master/SMAA.hlsl
SamplerState samPoint
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Mirror;
    AddressV = Mirror;
};

Texture2D gTexture;
Texture2D<uint> gEdges;
Texture2D<float> gDepthBuffer;
/// Create Depth Stencil State (ENABLE DEPTH WRITING)
DepthStencilState gDepth
{
    DEPTHWRITEMASK = zero;
    depthEnable = true;
};
/// Create Rasterizer State (Backface culling) 

RasterizerState gState
{
    CullMode = back;
};

BlendState edgeBlendState
{
    BlendEnable[0] = False;
};

BlendState antiAliasingBlendState
{
    BlendEnable[0] = TRUE;
    SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA;
};

//IN/OUT STRUCTS
//--------------
struct VS_INPUT
{
    float3 Position : POSITION;
    float2 TexCoord : TEXCOORD0;

};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
    float2 TexCoord : TEXCOORD1;
};

//VERTEX SHADER
//-------------
PS_INPUT VS(VS_INPUT input)
{
    PS_INPUT output = (PS_INPUT) 0;
	// Set the Position
	// Set the TexCoord
    output.Position = float4(input.Position, 1);
    output.TexCoord = input.TexCoord;
    return output;
}

float4 GetNeightbours(float2 uv, float2 offsets)
{
    float4 neightboursValues = (float4)0;
    neightboursValues.x = gDepthBuffer.Sample(samPoint, float2(uv.x + offsets.x, uv.y));
    neightboursValues.y = gDepthBuffer.Sample(samPoint, float2(uv.x, uv.y + offsets.y));
    neightboursValues.z = gDepthBuffer.Sample(samPoint, float2(uv.x - offsets.x, uv.y));
    neightboursValues.w = gDepthBuffer.Sample(samPoint, float2(uv.x, uv.y - offsets.y));
    return neightboursValues;
}
//PIXEL SHADER
//------------
uint IsEdge(PS_INPUT input) : SV_Target
{
    float depthThreshold = 0.0001f; // the minimum difference in depth before it's considered as an edge
    
    // we calculate the offsets needed in uv space to move 1 pixel
    uint width;
    uint height;
    gTexture.GetDimensions(width, height);
    float dx = 1.f / (float) width;
    float dy = 1.f / (float) height;
    
    // neighbours contains the depth values of the pixel to the left, to the top, to the right and to the bottom
    float4 neightbours = GetNeightbours(input.TexCoord, float2(dx, dy));
    // z is the depth value of the current pixel
    float z = gDepthBuffer.Sample(samPoint, input.TexCoord);
    // we calculate the difference in the depth to the current pixel
    float4 delta = abs(float4(z, z, z, z) - float4(neightbours.x, neightbours.y, neightbours.z, neightbours.w));
    // if depthThreshold is larger than delta it return 1
    float4 edges = step(depthThreshold, delta);
    
    // the dot product is (a.x * b.x) + (a.y * b.y)...
    // so if 1 of the values is 1 the result will not be 0 so we know it's an edge
    if (dot(edges, float4(1.f,1.f,1.f,1.f)) == 0.0f)
    {
        return 0;
    }
    else
    {
        return 1;
    }
    
}

float4 AntiAliasingPS(PS_INPUT input) : SV_Target
{
    
	// Step 1: find the dimensions of the texture (the texture has a method for that)	
    uint width;
    uint height;
    gTexture.GetDimensions(width, height);
	// Step 2: calculate dx and dy (UV space for 1 pixel)	
    float dx = 1.f / (float) width;
    float dy = 1.f / (float) height;
    

    float4 color = (float4)0;
    if (IsEdge(input))
    {
        [unroll] // these need to be unrolled for some reason
        for (int x = -1; x < 2; ++x)
        {
            [unroll]
            for (int y = -1; y < 2; ++y)
            {
                color += gTexture.Sample(samPoint, input.TexCoord + float2(2 * x * dx, 2 * y * dy));
            }
        }
        color /= 3 * 3;
    }
    else
    {
        color = gTexture.Sample(samPoint, input.TexCoord);
    }
    return color;
}

//TECHNIQUE
//---------
technique11 AntiAliasing
{
    pass AntiAliasing
    {
		// Set states...
        SetRasterizerState(gState);
        SetBlendState(antiAliasingBlendState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
        SetDepthStencilState(gDepth, 0);

        SetVertexShader(CompileShader(vs_4_0, VS()));
        SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, AntiAliasingPS()));
    }
}